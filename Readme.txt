/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

This SDK targets the board(s) pca10040.

Some applications uses GPIOs that may be different if you are using a different
board than the one specified above.

You can add a new board by defining your own board.h in board/<new_board>/board.h

This SDK contains multiple applications that you can build with the following
command:

	> make app_name=<name of the app folder under source folder> target_board=<name of the board>

Some application may not be available for some board (if a given sensor is not present for example).

Each application must have its own app_area_id. It allows multiple applications
to coexist in the same network and avoid mismatch during OTAP.
To modify the app_area_id of an application, app_specific_area_id variable must
be set in application config.mk file. It is a 24 bits variable and its most
significant bit must be one.

For example:
	app_specific_area_id=0x800001

The full app_area_id is a 32 bits value. The 8 less significant bits are
automatically added by the build system and dependant on the MCU. It avoids
mismatch during OTAP when updating network with different hardware.

List of application and their default area id:

 - nfc
	app_area_id = 0x81FAB803 compatible with ['pca10040'] 
 - inventory_app_router
	app_area_id = 0x84BEBB03 compatible with ['pca10040'] 
 - pwm_driver
	app_area_id = 0x8067A503 compatible with ['pca10040'] 
 - appconfig_app
	app_area_id = 0x84BEBD03 compatible with ['pca10040'] 
 - ble_app
	app_area_id = 0x5bde2a03 compatible with ['pca10040'] 
 - inventory_app_tag
	app_area_id = 0x84BEAA03 compatible with ['pca10040'] 
 - dualmcu_app
	app_area_id = 0x846B7403 compatible with ['pca10040'] 
 - custom_app
	app_area_id = 0x83744C03 compatible with ['pca10040'] 
 - basic_interrupt
	app_area_id = 0x81FD4103 compatible with ['pca10040'] 
 - minimal_app
	app_area_id = 0x8CEC7903 compatible with ['pca10040'] 
 - positioning_app
	app_area_id = 0x8010A003 compatible with ['pca10040'] 
 - blink
	app_area_id = 0x82407603 compatible with ['pca10040'] 
 - scheduler_example
	app_area_id = 0x12A20503 compatible with ['pca10040'] 
 - ble_beacon
	app_area_id = 0x823FCA03 compatible with ['pca10040']
 - migas-tr1-sink
	app_area_id = 0x846B7403 compatible with ['pca10040'] 
 - migas-tr1-node
	app_area_id = 0x83744C03 compatible with ['pca10040'] 
