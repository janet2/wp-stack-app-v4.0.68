var doublebuffer_8h =
[
    [ "double_buffer_t", "d0/d86/structdouble__buffer__t.html", "d0/d86/structdouble__buffer__t" ],
    [ "DoubleBuffer_getActive", "d7/df6/doublebuffer_8h.html#ae1386fe6b01598a8213fed1e9c231730", null ],
    [ "DoubleBuffer_getIndex", "d7/df6/doublebuffer_8h.html#a1c91e8765b182a8664b0cd1044de21ef", null ],
    [ "DoubleBuffer_incrIndex", "d7/df6/doublebuffer_8h.html#aad4131ee9fc237331f52b1dd69d22d06", null ],
    [ "DoubleBuffer_init", "d7/df6/doublebuffer_8h.html#a86966078df0a637a0e029b4ba44cd9cd", null ],
    [ "DoubleBuffer_swipe", "d7/df6/doublebuffer_8h.html#ab646d188197659ce70bcbf263e5098e4", null ]
];