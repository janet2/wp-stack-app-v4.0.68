var in6_8h =
[
    [ "in6_addr", "d4/d7d/structin6__addr.html", "d4/d7d/structin6__addr" ],
    [ "sockaddr_in6", "d0/d26/structsockaddr__in6.html", "d0/d26/structsockaddr__in6" ],
    [ "IN6_ADDR_SIZE", "d7/d27/in6_8h.html#aeeee89c2a2c4089e52136c9375bead54", null ],
    [ "IN6ADDR_ANY_INIT", "d7/d27/in6_8h.html#a1de876a356ee05a2e9427b741f99f49c", null ],
    [ "IN6ADDR_INTERFACELOCAL_ALLNODES_INIT", "d7/d27/in6_8h.html#ac948d0f1f609a601af9a694b2584879e", null ],
    [ "IN6ADDR_INTERFACELOCAL_ALLROUTERS_INIT", "d7/d27/in6_8h.html#aef2d9a20e0f18a1da3a2b51b2de0a4fb", null ],
    [ "IN6ADDR_LINKLOCAL_ALLNODES_INIT", "d7/d27/in6_8h.html#ad5e94c7f24e2e11eabcc8f26a8705508", null ],
    [ "IN6ADDR_LINKLOCAL_ALLROUTERS_INIT", "d7/d27/in6_8h.html#a8f17e3c9d73873d633c3d7788404dc15", null ],
    [ "IN6ADDR_LOOPBACK_INIT", "d7/d27/in6_8h.html#a5562c81af19ee5988ddc5a5c6153cf37", null ],
    [ "IN6ADDR_SITELOCAL_ALLROUTERS_INIT", "d7/d27/in6_8h.html#a8cbc1300338999caf5ce176803b677cf", null ],
    [ "ipv6_proto_numbers", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813", [
      [ "IP_PROTO_UNDEFINED", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a3cc311ffed2543d12b41c2a757ddf9b1", null ],
      [ "IP_PROTO_IP", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a0211e35f60a55e1708e2de0889a194eb", null ],
      [ "IP_PROTO_ICMP", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a8ad81700b9f61cb412f320e37d0a2cc5", null ],
      [ "IP_PROTO_UDP", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813ad1dccc22ac3b39ee92701bfeabe944b7", null ],
      [ "IP_PROTO_RESERVED", "d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a2be22166a50de02a713163ffef129f90", null ]
    ] ],
    [ "sockaddr_in6_match", "d7/d27/in6_8h.html#a8ecd5d49b80484ca5516e625eb6dc7ae", null ],
    [ "in6addr_any", "d7/d27/in6_8h.html#af8c97553060738d9edd6bfeab13ef7c3", null ],
    [ "in6addr_interfacelocal_allnodes", "d7/d27/in6_8h.html#aafd0275c579112c6b459023f5d05b2c0", null ],
    [ "in6addr_interfacelocal_allrouters", "d7/d27/in6_8h.html#ac8f9190ad245504adfb77b1fa691bada", null ],
    [ "in6addr_linklocal_allnodes", "d7/d27/in6_8h.html#ac725f6facdfb8270863bf319ae4800ea", null ],
    [ "in6addr_linklocal_allrouters", "d7/d27/in6_8h.html#a03c49ab9a94d01f4ba2f7bbfca6f7e6a", null ],
    [ "in6addr_loopback", "d7/d27/in6_8h.html#a530365cf9892a8eb72d6585614327ce8", null ],
    [ "in6addr_sitelocal_allrouters", "d7/d27/in6_8h.html#accf1352706d731049eb166f7f6a50d66", null ]
];