var structapp__lib__data__t =
[
    [ "allowReception", "d0/d3a/structapp__lib__data__t.html#aeea4fa805f730ad9ea1d5aa9eedcfce4", null ],
    [ "getAppConfigNumBytes", "d0/d3a/structapp__lib__data__t.html#a7880d731b35d4859af5cc03e532a5ee2", null ],
    [ "getDataMaxNumBytes", "d0/d3a/structapp__lib__data__t.html#af48d20fb67cf4c7e4815f8b37ee2f097", null ],
    [ "getMaxMsgQueuingTime", "d0/d3a/structapp__lib__data__t.html#a2e51edbb872e65675d25d46bc265692f", null ],
    [ "getNumBuffers", "d0/d3a/structapp__lib__data__t.html#af37e39667749f84966729293787d71a3", null ],
    [ "getNumFreeBuffers", "d0/d3a/structapp__lib__data__t.html#a3a269db0cadd55365431f63887ae9ff7", null ],
    [ "readAppConfig", "d0/d3a/structapp__lib__data__t.html#a1e36792f80a94d5021679ea631b58ff2", null ],
    [ "reserved", "d0/d3a/structapp__lib__data__t.html#a6e7e4bb07e5b35fccffc7f395bf4a3be", null ],
    [ "sendData", "d0/d3a/structapp__lib__data__t.html#ae5430956321eb31f198b6e2e85c327ed", null ],
    [ "setBcastDataReceivedCb", "d0/d3a/structapp__lib__data__t.html#a8f11310b68d39bd38735c51e629487e9", null ],
    [ "setDataReceivedCb", "d0/d3a/structapp__lib__data__t.html#a13489e95ff4f3593662f9d65b5239a85", null ],
    [ "setDataSentCb", "d0/d3a/structapp__lib__data__t.html#a9c1c70812e5a7dabd812d7aac97d63b7", null ],
    [ "setMaxMsgQueuingTime", "d0/d3a/structapp__lib__data__t.html#a9647003d430c89c1588937c8fa103273", null ],
    [ "setNewAppConfigCb", "d0/d3a/structapp__lib__data__t.html#af852205932b8d703c666eff4e45a0601", null ],
    [ "writeAppConfig", "d0/d3a/structapp__lib__data__t.html#ab4b6ec7182321ddc431a90098bb4b116", null ]
];