var structapp__lib__hardware__t =
[
    [ "activatePeripheral", "d0/d0c/structapp__lib__hardware__t.html#ab8499e0a734eb1486746371730740491", null ],
    [ "deactivatePeripheral", "d0/d0c/structapp__lib__hardware__t.html#a3f72a92f695a1cb928fa0da86c9ea8b4", null ],
    [ "isPeripheralActivated", "d0/d0c/structapp__lib__hardware__t.html#a52bb83766bb293021513905eda753fe5", null ],
    [ "readSupplyVoltage", "d0/d0c/structapp__lib__hardware__t.html#a79e4583a134b03f63dd541ffb1cbe6ee", null ],
    [ "readTemperature", "d0/d0c/structapp__lib__hardware__t.html#aa06b5d13c33a1c3cd7df67b377927cc4", null ],
    [ "releasePeripheral", "d0/d0c/structapp__lib__hardware__t.html#ac5dad7acc327d1478aa35be544c2efb0", null ],
    [ "reservePeripheral", "d0/d0c/structapp__lib__hardware__t.html#a57b0983135683001211206602c00fedb", null ]
];