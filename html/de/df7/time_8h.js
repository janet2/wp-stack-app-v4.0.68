var time_8h =
[
    [ "app_lib_time_t", "da/dda/structapp__lib__time__t.html", "da/dda/structapp__lib__time__t" ],
    [ "APP_LIB_TIME_NAME", "de/df7/time_8h.html#a5688d93e26d6e9e7335247cefce1e2fd", null ],
    [ "APP_LIB_TIME_VERSION", "de/df7/time_8h.html#acd27d981f5381a2e1b775de3e59b4ad4", null ],
    [ "app_lib_time_add_us_to_timestamp_hp_f", "de/df7/time_8h.html#a13f15fccd2aad6646bf2fd8baaac5f27", null ],
    [ "app_lib_time_get_max_delay_hp_us_f", "de/df7/time_8h.html#a4334a4edf34ae798d7029ddab92ba84d", null ],
    [ "app_lib_time_get_time_difference_us_f", "de/df7/time_8h.html#ad86f5c2fc4631d9f1154350fd12a31cd", null ],
    [ "app_lib_time_get_timestamp_coarse_f", "de/df7/time_8h.html#a211e02660a1fee3eed5e262eeb6aad48", null ],
    [ "app_lib_time_get_timestamp_hp_f", "de/df7/time_8h.html#ae59738d772f8bbaf5eb341459cdad2e0", null ],
    [ "app_lib_time_get_timestamp_s_f", "de/df7/time_8h.html#a6e4fcea347f7f7dab6d7036988f4c6fe", null ],
    [ "app_lib_time_is_timestamp_hp_before_f", "de/df7/time_8h.html#a6a38495b9bcfae2fea9caefb9ebfdb8d", null ],
    [ "app_lib_time_timestamp_coarse_t", "de/df7/time_8h.html#a15e994fb5d9a74436a5d4771b4e13d13", null ],
    [ "app_lib_time_timestamp_hp_t", "de/df7/time_8h.html#a46366e00a718eae381bc08dc4e3c2bf2", null ]
];