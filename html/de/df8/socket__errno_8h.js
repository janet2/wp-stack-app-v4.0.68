var socket__errno_8h =
[
    [ "socket_errno", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8", [
      [ "S_EPERM", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8abe8f03c69babeacb6aef9a33c7667608", null ],
      [ "S_ENOENT", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8afdb695424856717dbce28783134ebf85", null ],
      [ "S_EIO", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8acdb7bbc0aa3e296f8242d1502d7e6c9c", null ],
      [ "S_ENXIO", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8ac7623da5279c17be6282dbe4b8d305ed", null ],
      [ "S_EADDRINUSE", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a4fe1e3c11e2878e952a1f25ab13048cb", null ],
      [ "S_EBADF", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a920f82fa35e4f239e5c9d2040d4a1534", null ],
      [ "S_EAGAIN", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a2efdaca255f1423b33211acbbb699798", null ],
      [ "S_ENOMEM", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8ad50b23975b6ae5caeadc2f690dc0c19b", null ],
      [ "S_EACCES", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a88b7277b4289d3f62b2c6f873a582c47", null ],
      [ "S_EFAULT", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a45269f485a25a3b9989774059c2b69e5", null ],
      [ "S_EBUSY", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8ae19dbfc0196452f2cdba299ea9cea05b", null ],
      [ "S_ENODEV", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8aa18bc1960a2ec395ab99c677f9fd68c8", null ],
      [ "S_EINVAL", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a29ef1f63469d15e943af702bb858a541", null ],
      [ "S_ENFILE", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a29ae0282549e2c0df934aa2e801d4efb", null ],
      [ "S_EMFILE", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a1e107dac36dca175650308de62b8131c", null ],
      [ "S_ENOSPC", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a975e3660c23c8c74783eaee66924451e", null ],
      [ "S_EPROTO", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a3a59024fea8bc601fd97f7f6c038daa8", null ],
      [ "S_ENOPROTOOPT", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a659fa20db901ba66157137254090c063", null ],
      [ "S_EPROTONOSUPPORT", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a6e7c7bf219b797a64725b704da8c1555", null ],
      [ "S_EAFNOSUPPORT", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8ab35ce69a9695b73c721bc232fe560104", null ],
      [ "S_EADDRNOTAVAIL", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8ae6a7a66fcf89d3f5e5c3e4630ceccbaf", null ],
      [ "S_EDESTADDRREQ", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8abe9caef9b9cdf8d9c93bb166f7cac91f", null ],
      [ "S_FORCE_U32", "de/df8/socket__errno_8h.html#a5f4acb20d7ea97cc8c8248adbd5061a8a6cb364b3fcf3bebb2a8f6bbf7889379a", null ]
    ] ]
];