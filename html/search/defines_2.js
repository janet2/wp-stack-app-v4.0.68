var searchData=
[
  ['board_5fbutton_5factive_5flow',['BOARD_BUTTON_ACTIVE_LOW',['../d3/d02/board_8h.html#a22dc8bf341319e58e0377f0a58770d71',1,'board.h']]],
  ['board_5fbutton_5fpin_5flist',['BOARD_BUTTON_PIN_LIST',['../d3/d02/board_8h.html#ac206a3fdd4723341044664ddbf85f6d3',1,'board.h']]],
  ['board_5fled_5fpin_5flist',['BOARD_LED_PIN_LIST',['../d3/d02/board_8h.html#a8eefc53de69e1aea7376f76b67c6052a',1,'board.h']]],
  ['board_5fpwm_5foutput_5fgpio',['BOARD_PWM_OUTPUT_GPIO',['../d3/d02/board_8h.html#a983e7c69feec52c6fd3c9aa0b1bc11d2',1,'board.h']]],
  ['board_5fsupport_5fdcdc',['BOARD_SUPPORT_DCDC',['../d3/d02/board_8h.html#a0fdc2f8cf1af139317a899ae118066f8',1,'board.h']]],
  ['board_5fuart_5firq_5fpin',['BOARD_UART_IRQ_PIN',['../d3/d02/board_8h.html#a86821c228bc6ae6867d9a724553e8ecc',1,'board.h']]],
  ['board_5fusart_5fcts_5fpin',['BOARD_USART_CTS_PIN',['../d3/d02/board_8h.html#a73ea4f5228391904dcf748e08f98a172',1,'board.h']]],
  ['board_5fusart_5frts_5fpin',['BOARD_USART_RTS_PIN',['../d3/d02/board_8h.html#a0af72e93971553bc01429840dbcede95',1,'board.h']]],
  ['board_5fusart_5frx_5fpin',['BOARD_USART_RX_PIN',['../d3/d02/board_8h.html#a29ae5883a2b90d5dd60552e78fa1a639',1,'board.h']]],
  ['board_5fusart_5ftx_5fpin',['BOARD_USART_TX_PIN',['../d3/d02/board_8h.html#ac5c54565e5266fd2bf7f9b8bace2ae9c',1,'board.h']]]
];
