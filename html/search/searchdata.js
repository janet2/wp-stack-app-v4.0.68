var indexSectionsWithContent =
{
  0: "_abcdefghilmnopqrstuvw",
  1: "acdirst",
  2: "abcdhilmnoprstu",
  3: "_abcdghilmoprstu",
  4: "_abcdefghilmnopqrstuvw",
  5: "aghost",
  6: "abhilpstu",
  7: "abhilpstu",
  8: "_abcdegilmprsu",
  9: "ahsw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

