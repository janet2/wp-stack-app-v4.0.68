var searchData=
[
  ['api_5fopen',['API_Open',['../dc/d65/api_8h.html#abea8e1483d9fe0c251a2027a34422031',1,'api.h']]],
  ['app_5fentrypoint',['App_entrypoint',['../d2/d39/app_8h.html#aeac7453aac5efa0b1d5ae5b34b8166cd',1,'app.h']]],
  ['app_5fgetapiversion',['App_getApiVersion',['../d2/d39/app_8h.html#a79387855d9e2d22902b543a2cd0309bd',1,'app.h']]],
  ['app_5flib_5fsettings_5fcreate_5frole',['app_lib_settings_create_role',['../de/d60/settings_8h.html#a4f7704544bbf5a456b9f21f55ca42822',1,'settings.h']]],
  ['app_5flib_5fsettings_5fget_5fbase_5frole',['app_lib_settings_get_base_role',['../de/d60/settings_8h.html#ae6aef3b74883ee5ba5b8debaeda8782d',1,'settings.h']]],
  ['app_5flib_5fsettings_5fget_5fflags_5frole',['app_lib_settings_get_flags_role',['../de/d60/settings_8h.html#aa36cbec6b2d7431dc327dea294da1185',1,'settings.h']]],
  ['app_5fscheduler_5faddtask',['App_Scheduler_addTask',['../dd/ddc/app__scheduler_8h.html#ab4edf0927cbbbf8e09ba6f59698b98f2',1,'app_scheduler.h']]],
  ['app_5fscheduler_5fcanceltask',['App_Scheduler_cancelTask',['../dd/ddc/app__scheduler_8h.html#acd03212a86359f0f62d4953785087fd2',1,'app_scheduler.h']]],
  ['app_5fscheduler_5finit',['App_Scheduler_init',['../dd/ddc/app__scheduler_8h.html#aadb275bc1de9c05d677ab56117535802',1,'app_scheduler.h']]],
  ['appconfig_5fgetdata',['AppConfig_getData',['../da/d88/app__config_8h.html#af621c93e8a11f87015b9e68f8327d7d3',1,'app_config.h']]]
];
