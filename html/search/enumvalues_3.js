var searchData=
[
  ['i2c_5fres_5falready_5finitialized',['I2C_RES_ALREADY_INITIALIZED',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ab938520f0ffca1688575ee4cea743f62',1,'i2c.h']]],
  ['i2c_5fres_5fanack',['I2C_RES_ANACK',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a9d1857804d7522ef1bf32b43108904d2',1,'i2c.h']]],
  ['i2c_5fres_5fbus_5fhang',['I2C_RES_BUS_HANG',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a0d9c0111010184dc8851319bcf212ec5',1,'i2c.h']]],
  ['i2c_5fres_5fbusy',['I2C_RES_BUSY',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ac922a564540c0f2567f32b53610e9c15',1,'i2c.h']]],
  ['i2c_5fres_5fdnack',['I2C_RES_DNACK',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214aa7eaa92ebe1d4e25dc5e4710e9353197',1,'i2c.h']]],
  ['i2c_5fres_5finvalid_5fconfig',['I2C_RES_INVALID_CONFIG',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a16ef0218b1ea2424fc9ead64d33d766e',1,'i2c.h']]],
  ['i2c_5fres_5finvalid_5fxfer',['I2C_RES_INVALID_XFER',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a31ff3e53b9b26face48fce20b8748edf',1,'i2c.h']]],
  ['i2c_5fres_5fnot_5finitialized',['I2C_RES_NOT_INITIALIZED',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ac75e231b366ef0e73af48bed4cf5e42d',1,'i2c.h']]],
  ['i2c_5fres_5fok',['I2C_RES_OK',['../d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a71e46a1d7b21b703c9ea82225e5ffbd4',1,'i2c.h']]],
  ['ip_5fproto_5ficmp',['IP_PROTO_ICMP',['../d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a8ad81700b9f61cb412f320e37d0a2cc5',1,'in6.h']]],
  ['ip_5fproto_5fip',['IP_PROTO_IP',['../d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a0211e35f60a55e1708e2de0889a194eb',1,'in6.h']]],
  ['ip_5fproto_5freserved',['IP_PROTO_RESERVED',['../d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a2be22166a50de02a713163ffef129f90',1,'in6.h']]],
  ['ip_5fproto_5fudp',['IP_PROTO_UDP',['../d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813ad1dccc22ac3b39ee92701bfeabe944b7',1,'in6.h']]],
  ['ip_5fproto_5fundefined',['IP_PROTO_UNDEFINED',['../d7/d27/in6_8h.html#a6d1425f7765226d636f99915d1b6a813a3cc311ffed2543d12b41c2a757ddf9b1',1,'in6.h']]]
];
