var searchData=
[
  ['read',['read',['../da/d23/structapp__lib__otap__t.html#a4c4b909f059c1f84692494a0cdd5c957',1,'app_lib_otap_t']]],
  ['read_5fptr',['read_ptr',['../df/d55/structi2c__xfer__t.html#a3819982be3076624007198e0c02cd02c',1,'i2c_xfer_t::read_ptr()'],['../d7/d6d/structspi__xfer__t.html#a3819982be3076624007198e0c02cd02c',1,'spi_xfer_t::read_ptr()']]],
  ['read_5fsize',['read_size',['../df/d55/structi2c__xfer__t.html#a7164bd664ab18ea8f706b9a2fad88d79',1,'i2c_xfer_t::read_size()'],['../d7/d6d/structspi__xfer__t.html#a7164bd664ab18ea8f706b9a2fad88d79',1,'spi_xfer_t::read_size()']]],
  ['readappconfig',['readAppConfig',['../d0/d3a/structapp__lib__data__t.html#a1e36792f80a94d5021679ea631b58ff2',1,'app_lib_data_t']]],
  ['readpersistent',['readPersistent',['../d3/d48/structapp__lib__storage__t.html#a1d7eeed1491269347213f7db9f2776a6',1,'app_lib_storage_t']]],
  ['readsupplyvoltage',['readSupplyVoltage',['../d0/d0c/structapp__lib__hardware__t.html#a79e4583a134b03f63dd541ffb1cbe6ee',1,'app_lib_hardware_t']]],
  ['readtemperature',['readTemperature',['../d0/d0c/structapp__lib__hardware__t.html#aa06b5d13c33a1c3cd7df67b377927cc4',1,'app_lib_hardware_t']]],
  ['recvfrom',['recvfrom',['../da/def/structapp__lib__ipv6__t.html#a5f85fe8b047c7b19a885fa2bd9b787a7',1,'app_lib_ipv6_t']]],
  ['registergroupquery',['registerGroupQuery',['../de/d5b/structapp__lib__settings__t.html#a4f6e72a53bbd71152ea996bedfa29785',1,'app_lib_settings_t']]],
  ['releaseperipheral',['releasePeripheral',['../d0/d0c/structapp__lib__hardware__t.html#ac5dad7acc327d1478aa35be544c2efb0',1,'app_lib_hardware_t']]],
  ['reserved',['reserved',['../d0/d3a/structapp__lib__data__t.html#a6e7e4bb07e5b35fccffc7f395bf4a3be',1,'app_lib_data_t']]],
  ['reserveperipheral',['reservePeripheral',['../d0/d0c/structapp__lib__hardware__t.html#a57b0983135683001211206602c00fedb',1,'app_lib_hardware_t']]],
  ['resetall',['resetAll',['../de/d5b/structapp__lib__settings__t.html#a9a6ef9b9b9a423c397a6a0c4e10eada3',1,'app_lib_settings_t']]],
  ['rssi',['rssi',['../d3/d1f/structapp__lib__beacon__rx__received__t.html#afb67d818cd76cce8057affabcb1979a6',1,'app_lib_beacon_rx_received_t::rssi()'],['../d7/dc8/structapp__lib__state__beacon__rx__t.html#ac4ea00add191a3290e428c5fdc838d1f',1,'app_lib_state_beacon_rx_t::rssi()']]],
  ['rx_5fpower',['rx_power',['../d1/db3/structapp__lib__state__nbor__info__t.html#a5394085ff97c98685b8c004dfac8c229',1,'app_lib_state_nbor_info_t']]]
];
