var otap_8h =
[
    [ "app_lib_otap_remote_status_t", "d2/dd9/structapp__lib__otap__remote__status__t.html", "d2/dd9/structapp__lib__otap__remote__status__t" ],
    [ "app_lib_otap_t", "da/d23/structapp__lib__otap__t.html", "da/d23/structapp__lib__otap__t" ],
    [ "APP_LIB_OTAP_NAME", "db/de4/otap_8h.html#a67bc5d08dddcd410da756139f8669c34", null ],
    [ "APP_LIB_OTAP_VERSION", "db/de4/otap_8h.html#ae2145d18f58336cda7965ce108ce40d8", null ],
    [ "app_lib_otap_begin_f", "db/de4/otap_8h.html#a2a9452ad122f9e0eb785ccf3127eb6b9", null ],
    [ "app_lib_otap_clear_f", "db/de4/otap_8h.html#a5de1b1fcb3ae5a93c4ca05fbdc8f5377", null ],
    [ "app_lib_otap_get_crc_f", "db/de4/otap_8h.html#a717bad2d59431cdaee79d10651a37eb0", null ],
    [ "app_lib_otap_get_max_block_num_bytes_f", "db/de4/otap_8h.html#ae924632c04983cb6ba2afe3a8b694a0a", null ],
    [ "app_lib_otap_get_max_num_bytes_f", "db/de4/otap_8h.html#a13e435170e4c33979f73eeb96cda027d", null ],
    [ "app_lib_otap_get_num_bytes_f", "db/de4/otap_8h.html#aafe82b5a7cde325d79597a32a8f01a42", null ],
    [ "app_lib_otap_get_processed_area_id_f", "db/de4/otap_8h.html#a987f1232f8b254fe221f6164c12e6aca", null ],
    [ "app_lib_otap_get_processed_crc_f", "db/de4/otap_8h.html#ad40a148fe865cabe1333e1f7c7d2a61e", null ],
    [ "app_lib_otap_get_processed_num_bytes_f", "db/de4/otap_8h.html#aabddb4f821692d1bcd46abf462dae3bf", null ],
    [ "app_lib_otap_get_processed_seq_f", "db/de4/otap_8h.html#ac0f5a9e8d02898778aace85aa365efdd", null ],
    [ "app_lib_otap_get_seq_f", "db/de4/otap_8h.html#aafa8cb3e2572d54347c025cf92613067", null ],
    [ "app_lib_otap_get_status_f", "db/de4/otap_8h.html#a32b38d7421cfaf79dbcfbd92e88d2db7", null ],
    [ "app_lib_otap_get_type_f", "db/de4/otap_8h.html#a036945238f9eb0113c4a7d7d6c58e295", null ],
    [ "app_lib_otap_is_processed_f", "db/de4/otap_8h.html#a087320078aae3cb785f4650b75aceb23", null ],
    [ "app_lib_otap_is_set_to_be_processed_f", "db/de4/otap_8h.html#a0b17b78fcade9f9356a337b9653728a2", null ],
    [ "app_lib_otap_is_valid_f", "db/de4/otap_8h.html#abdf9863f1ed350eee6b670c773532044", null ],
    [ "app_lib_otap_read_f", "db/de4/otap_8h.html#ae18b5c19d96a4ba019861bca3d0ee088", null ],
    [ "app_lib_otap_remote_status_received_cb_f", "db/de4/otap_8h.html#a6348f8ca30a4cad3dad6b16341496fff", null ],
    [ "app_lib_otap_send_remote_status_req_f", "db/de4/otap_8h.html#a6417d8d33bbd51d4fb03f9ea81f19158", null ],
    [ "app_lib_otap_send_remote_update_req_f", "db/de4/otap_8h.html#a26a7bf22f473fbf232a0399b96bf3542", null ],
    [ "app_lib_otap_seq_t", "db/de4/otap_8h.html#a9210ee2b1fcb18dec3ff82f2b40a6042", null ],
    [ "app_lib_otap_set_remote_status_received_cb_f", "db/de4/otap_8h.html#af3730713a4017229e812d1a984a02a79", null ],
    [ "app_lib_otap_set_to_be_processed_f", "db/de4/otap_8h.html#a4f0f4fe73f06dd4188dfa5afe6d1b15c", null ],
    [ "app_lib_otap_write_f", "db/de4/otap_8h.html#a4ddcacb42e945d34f805fc347d5ccc12", null ],
    [ "app_lib_otap_status_e", "db/de4/otap_8h.html#ac41b0b1ca21d9057ba5aaa3db7d44f3e", [
      [ "APP_LIB_OTAP_STATUS_OK", "db/de4/otap_8h.html#ac41b0b1ca21d9057ba5aaa3db7d44f3ea030ffd68eb5bb7cf82a6348bb47f8508", null ],
      [ "APP_LIB_OTAP_STATUS_NEW", "db/de4/otap_8h.html#ac41b0b1ca21d9057ba5aaa3db7d44f3ea454d62a18b15c9250e88b01673fd8840", null ]
    ] ],
    [ "app_lib_otap_type_e", "db/de4/otap_8h.html#a032ff8b703195395f090a4686a0f09e8", [
      [ "APP_LIB_OTAP_TYPE_BLANK", "db/de4/otap_8h.html#a032ff8b703195395f090a4686a0f09e8a696b2ce9bdf70bdbee1f967187d7f578", null ],
      [ "APP_LIB_OTAP_TYPE_PRESENT", "db/de4/otap_8h.html#a032ff8b703195395f090a4686a0f09e8a95b720bb07ca50cd90184eb77338e1af", null ],
      [ "APP_LIB_OTAP_TYPE_PROCESS", "db/de4/otap_8h.html#a032ff8b703195395f090a4686a0f09e8aa4dc706a3cadc123ff6e84779d4485ad", null ]
    ] ],
    [ "app_lib_otap_write_res_e", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dad", [
      [ "APP_LIB_OTAP_WRITE_RES_OK", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dadafed523c041d64364f9cd8f1d6225e916", null ],
      [ "APP_LIB_OTAP_WRITE_RES_COMPLETED_OK", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dada9782f02b79b25578f2c69affb7082e6a", null ],
      [ "APP_LIB_OTAP_WRITE_RES_COMPLETED_ERROR", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dadaeb53e76c1fbff9d319f1e189ec68941e", null ],
      [ "APP_LIB_OTAP_WRITE_RES_NOT_ONGOING", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dada29d8ba70e6268e2ddb3d8bfa221360c5", null ],
      [ "APP_LIB_OTAP_WRITE_RES_INVALID_START", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dadaf4d569acb804da0df36a5b87ef2d0cb3", null ],
      [ "APP_LIB_OTAP_WRITE_RES_INVALID_NUM_BYTES", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dadaed2218e608d59ca290208f2a6e246065", null ],
      [ "APP_LIB_OTAP_WRITE_RES_INVALID_HEADER", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dadafb8e793176b07414a2c221294e98d64d", null ],
      [ "APP_LIB_OTAP_WRITE_RES_INVALID_NULL_BYTES", "db/de4/otap_8h.html#a49a69b2ae836dbe4e510518bae2f3dada46a28fbc635dd3baabb2ff0340f0672a", null ]
    ] ]
];