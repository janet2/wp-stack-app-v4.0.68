var structapp__lib__system__t =
[
    [ "clearPendingFastAppIrq", "d3/dd6/structapp__lib__system__t.html#a67b4efddde8ecebedbac2d33796527fd", null ],
    [ "disableAppIrq", "d3/dd6/structapp__lib__system__t.html#aff602c2b7c9b89fd1dc54e1091310af2", null ],
    [ "disableDeepSleep", "d3/dd6/structapp__lib__system__t.html#aa2438041c696e81028c24825c32a1992", null ],
    [ "enableAppIrq", "d3/dd6/structapp__lib__system__t.html#a38f56116fe3b1384c1cd339f1277b31a", null ],
    [ "enterCriticalSection", "d3/dd6/structapp__lib__system__t.html#a418b7095a0b302c275075ff3c88c49dd", null ],
    [ "exitCriticalSection", "d3/dd6/structapp__lib__system__t.html#a5f90bd3899b516510f41c8faeb95767a", null ],
    [ "getBootloaderVersion", "d3/dd6/structapp__lib__system__t.html#abc6890be58fc8e39cea4495dadea7a63", null ],
    [ "getRadioInfo", "d3/dd6/structapp__lib__system__t.html#a848e4dc4fbcfb0eb086d08c3175b552d", null ],
    [ "setPeriodicCb", "d3/dd6/structapp__lib__system__t.html#a7a48086cf5189cf13f4e67f569977d7e", null ],
    [ "setShutdownCb", "d3/dd6/structapp__lib__system__t.html#a36577f886a9aec8b4669303f150f36f4", null ],
    [ "setStartupCb", "d3/dd6/structapp__lib__system__t.html#aa56da74fee961e058c91425d1625eaf3", null ]
];