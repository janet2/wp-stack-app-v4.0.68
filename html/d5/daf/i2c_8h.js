var i2c_8h =
[
    [ "i2c_conf_t", "db/dd0/structi2c__conf__t.html", "db/dd0/structi2c__conf__t" ],
    [ "i2c_xfer_t", "df/d55/structi2c__xfer__t.html", "df/d55/structi2c__xfer__t" ],
    [ "on_transfer_done_cb_f", "d5/daf/i2c_8h.html#a1f5df7e338ccc068a50ddd177e5e81c1", null ],
    [ "i2c_res_e", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214", [
      [ "I2C_RES_OK", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a71e46a1d7b21b703c9ea82225e5ffbd4", null ],
      [ "I2C_RES_INVALID_CONFIG", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a16ef0218b1ea2424fc9ead64d33d766e", null ],
      [ "I2C_RES_INVALID_XFER", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a31ff3e53b9b26face48fce20b8748edf", null ],
      [ "I2C_RES_NOT_INITIALIZED", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ac75e231b366ef0e73af48bed4cf5e42d", null ],
      [ "I2C_RES_ALREADY_INITIALIZED", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ab938520f0ffca1688575ee4cea743f62", null ],
      [ "I2C_RES_BUSY", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214ac922a564540c0f2567f32b53610e9c15", null ],
      [ "I2C_RES_ANACK", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a9d1857804d7522ef1bf32b43108904d2", null ],
      [ "I2C_RES_DNACK", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214aa7eaa92ebe1d4e25dc5e4710e9353197", null ],
      [ "I2C_RES_BUS_HANG", "d5/daf/i2c_8h.html#a1af2d3c652ac76ea124d7b5156714214a0d9c0111010184dc8851319bcf212ec5", null ]
    ] ],
    [ "I2C_close", "d5/daf/i2c_8h.html#a072c90996b70c49633424fb9d6aa9316", null ],
    [ "I2C_init", "d5/daf/i2c_8h.html#af0422f166c41410bd181d81b96314f1f", null ],
    [ "I2C_status", "d5/daf/i2c_8h.html#a03988ab797ee5f7e6094bc8271f432dd", null ],
    [ "I2C_transfer", "d5/daf/i2c_8h.html#aefdece61dcc99a2f1f94e126daf21c6f", null ]
];