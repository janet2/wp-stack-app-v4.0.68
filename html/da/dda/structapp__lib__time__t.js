var structapp__lib__time__t =
[
    [ "addUsToHpTimestamp", "da/dda/structapp__lib__time__t.html#a272a5070c94b8c80d559438a6ad17507", null ],
    [ "getMaxHpDelay", "da/dda/structapp__lib__time__t.html#a39d9aadd88553534e01c098bb48b7910", null ],
    [ "getTimeDiffUs", "da/dda/structapp__lib__time__t.html#a7515c132924aebba5ce8dc4a8c9a8101", null ],
    [ "getTimestampCoarse", "da/dda/structapp__lib__time__t.html#ae0f204846f7e79188f3d97332755f49d", null ],
    [ "getTimestampHp", "da/dda/structapp__lib__time__t.html#a26c930bc9424a23c4c82b73dc5ab4d6f", null ],
    [ "getTimestampS", "da/dda/structapp__lib__time__t.html#a4ed2bea71af509fda244e031e7073bf9", null ],
    [ "isHpTimestampBefore", "da/dda/structapp__lib__time__t.html#a546d6b21857d771894a4f5687f858c04", null ]
];