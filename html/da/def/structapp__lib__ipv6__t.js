var structapp__lib__ipv6__t =
[
    [ "bind", "da/def/structapp__lib__ipv6__t.html#a5c89391565eaf6a917c03be6693a4b57", null ],
    [ "close", "da/def/structapp__lib__ipv6__t.html#a0cb4fb76bce7a299370d6d568baefb16", null ],
    [ "getErrno", "da/def/structapp__lib__ipv6__t.html#a5f9a48e9147d15507c5175c756b09079", null ],
    [ "recvfrom", "da/def/structapp__lib__ipv6__t.html#a5f85fe8b047c7b19a885fa2bd9b787a7", null ],
    [ "sendto", "da/def/structapp__lib__ipv6__t.html#a01f446866b373bdd44f85f1d30033e7c", null ],
    [ "socket", "da/def/structapp__lib__ipv6__t.html#a46af8d475a05a20a6eaef29a8ae5b003", null ]
];