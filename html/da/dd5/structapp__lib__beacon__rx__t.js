var structapp__lib__beacon__rx__t =
[
    [ "isScannerStarted", "da/dd5/structapp__lib__beacon__rx__t.html#a7b07c00bdcda35470ab71a9c73bba53c", null ],
    [ "setBeaconReceivedCb", "da/dd5/structapp__lib__beacon__rx__t.html#a3825cd0cfc978cd52d956c6195604797", null ],
    [ "startScanner", "da/dd5/structapp__lib__beacon__rx__t.html#a30acac0d12cb85aa24292f214bd0a17e", null ],
    [ "stopScanner", "da/dd5/structapp__lib__beacon__rx__t.html#a0bb91e298163edbed33bd688612699db", null ]
];