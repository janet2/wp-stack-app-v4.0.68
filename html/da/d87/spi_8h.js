var spi_8h =
[
    [ "spi_conf_t", "d3/dbb/structspi__conf__t.html", "d3/dbb/structspi__conf__t" ],
    [ "spi_xfer_t", "d7/d6d/structspi__xfer__t.html", "d7/d6d/structspi__xfer__t" ],
    [ "on_transfer_done_cb_f", "da/d87/spi_8h.html#aefde22821cbd8911c1502993ab9d4ec2", null ],
    [ "spi_bit_order_e", "da/d87/spi_8h.html#ae4797b549e4bf5bb099063dbc87764b9", [
      [ "SPI_ORDER_MSB", "da/d87/spi_8h.html#ae4797b549e4bf5bb099063dbc87764b9a181c9a9eed2847edfaba2cc1e4daff94", null ],
      [ "SPI_ORDER_LSB", "da/d87/spi_8h.html#ae4797b549e4bf5bb099063dbc87764b9a3300ff542bb03e21abbcefbcb359c490", null ]
    ] ],
    [ "spi_mode_e", "da/d87/spi_8h.html#a0cb467f85c0d7ce489b861ee7bb20e9a", [
      [ "SPI_MODE_LOW_FIRST", "da/d87/spi_8h.html#a0cb467f85c0d7ce489b861ee7bb20e9aaa4f44cbc69625c3d88c89a5b56c23495", null ],
      [ "SPI_MODE_LOW_SECOND", "da/d87/spi_8h.html#a0cb467f85c0d7ce489b861ee7bb20e9aa9c6197837e74ec92f3f8a483993c01ad", null ],
      [ "SPI_MODE_HIGH_FIRST", "da/d87/spi_8h.html#a0cb467f85c0d7ce489b861ee7bb20e9aa7aebb9dcd5e5c35a51fdf2c880995ffb", null ],
      [ "SPI_MODE_HIGH_SECOND", "da/d87/spi_8h.html#a0cb467f85c0d7ce489b861ee7bb20e9aa908e2eb5d2b156236d08bf28c0ba9ce5", null ]
    ] ],
    [ "spi_res_e", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1a", [
      [ "SPI_RES_OK", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aaab387de120e46a0cfb9f4c1fa310f8f9", null ],
      [ "SPI_RES_INVALID_CONFIG", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aa246ea94340687ac1f94847d0ea9126a4", null ],
      [ "SPI_RES_INVALID_XFER", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aa3ca25746543d4c35e2d518bcfd1d6c9d", null ],
      [ "SPI_RES_NOT_INITIALIZED", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aaa7cac464a3cc1549a45375a75840a4f2", null ],
      [ "SPI_RES_ALREADY_INITIALIZED", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aa6565509a26e74b3203119cb901c3b2b1", null ],
      [ "SPI_RES_BUSY", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aaafd37191b3a0bb84966ac9a9ddb22b72", null ],
      [ "SPI_RES_BLOCKING_NOT_AVAILABLE", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aaeae268242c54efa6d4d84a5736a34c72", null ],
      [ "SPI_RES_ONLY_BLOCKING_AVAILABLE", "da/d87/spi_8h.html#aa73eafab9cd3df3fac6d8d856c0eea1aa47d489725753ccf50999ec6254973149", null ]
    ] ],
    [ "SPI_close", "da/d87/spi_8h.html#aba238cfdaf22ab5d72f5ddc1985843a0", null ],
    [ "SPI_init", "da/d87/spi_8h.html#aa94843701f8bd5ec702e0b775fdfd0c8", null ],
    [ "SPI_transfer", "da/d87/spi_8h.html#a9cd392449cad8123749951ea2e434415", null ]
];