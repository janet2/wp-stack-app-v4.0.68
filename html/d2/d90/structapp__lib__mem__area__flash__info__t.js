var structapp__lib__mem__area__flash__info__t =
[
    [ "byte_write_call_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#a8fb09e00542be47a78c14dfa208e4c1e", null ],
    [ "byte_write_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#a5c8c5fbb4b56f5d44cb03b3cdf30749f", null ],
    [ "erase_sector_size", "d2/d90/structapp__lib__mem__area__flash__info__t.html#af7004f5b20e3ba6d15d646de90981cb6", null ],
    [ "is_busy_call_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#a3cce340593a310f60cbd24a2e611c0bf", null ],
    [ "page_write_call_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#ae02544f600ba251e1c03712b2fce90ff", null ],
    [ "page_write_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#a9fc0b8defd420744f18e5ee478f0af6b", null ],
    [ "sector_erase_call_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#ad16d4dc3fe2433035df69fc4359355fe", null ],
    [ "sector_erase_time", "d2/d90/structapp__lib__mem__area__flash__info__t.html#ad8bef042481e7be25f2152ae5fcb025b", null ],
    [ "write_alignment", "d2/d90/structapp__lib__mem__area__flash__info__t.html#aaae3f76bb2618fc8b0ec8581339e329e", null ],
    [ "write_page_size", "d2/d90/structapp__lib__mem__area__flash__info__t.html#a7620fa44fbe0000c302271facd29eef4", null ]
];