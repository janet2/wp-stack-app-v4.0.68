var data_8h =
[
    [ "app_lib_data_received_t", "d7/df3/structapp__lib__data__received__t.html", "d7/df3/structapp__lib__data__received__t" ],
    [ "app_lib_data_to_send_t", "d7/d10/structapp__lib__data__to__send__t.html", "d7/d10/structapp__lib__data__to__send__t" ],
    [ "app_lib_data_sent_status_t", "d6/de0/structapp__lib__data__sent__status__t.html", "d6/de0/structapp__lib__data__sent__status__t" ],
    [ "app_lib_data_t", "d0/d3a/structapp__lib__data__t.html", "d0/d3a/structapp__lib__data__t" ],
    [ "APP_LIB_DATA_MAX_APP_CONFIG_NUM_BYTES", "d2/dbd/data_8h.html#ad30da92f672d77497c900d927d198359", null ],
    [ "APP_LIB_DATA_NAME", "d2/dbd/data_8h.html#a443fb0b86da06d9d1d72b6638e2fff98", null ],
    [ "APP_LIB_DATA_NO_TRACKING_ID", "d2/dbd/data_8h.html#a6a2e44ccee2f7e4393d38f9072bd1841", null ],
    [ "APP_LIB_DATA_RX_HOPS_UNDETERMINED", "d2/dbd/data_8h.html#a0f3dacc498ae65700f681eaa5d103623", null ],
    [ "APP_LIB_DATA_VERSION", "d2/dbd/data_8h.html#a137d61eb26723cafd60696afd00ba0f2", null ],
    [ "app_lib_data_allow_reception_f", "d2/dbd/data_8h.html#abb2289fea975f61fe189f4310578c0df", null ],
    [ "app_lib_data_bcast_data_received_cb_f", "d2/dbd/data_8h.html#a77930a18346d062fb21c9b5645076b75", null ],
    [ "app_lib_data_data_received_cb_f", "d2/dbd/data_8h.html#a7d8adb4ccf27eed17ff4b39ef26c6ab7", null ],
    [ "app_lib_data_data_sent_cb_f", "d2/dbd/data_8h.html#ad4532c2af19fb5161fd44dd3d806be38", null ],
    [ "app_lib_data_get_app_config_num_bytes_f", "d2/dbd/data_8h.html#a37af27daa2e4777b89b13f2f8440b8b9", null ],
    [ "app_lib_data_get_data_max_num_bytes_f", "d2/dbd/data_8h.html#a2cefa4eedd71e2cce843f12f96881c51", null ],
    [ "app_lib_data_get_max_msg_queuing_time_f", "d2/dbd/data_8h.html#a48bd185fa7c62fe0f95092fa45dc21cf", null ],
    [ "app_lib_data_get_num_buffers_f", "d2/dbd/data_8h.html#a58aea42da58d6bdbeb88ec0c5d6a1023", null ],
    [ "app_lib_data_get_num_free_buffers_f", "d2/dbd/data_8h.html#a562531defac3f48df9019556d1771043", null ],
    [ "app_lib_data_new_app_config_cb_f", "d2/dbd/data_8h.html#aca1298154f7a3d7be77c64ca602639ed", null ],
    [ "app_lib_data_read_app_config_f", "d2/dbd/data_8h.html#a96f7b21d1f7992439c9526b3a4d27530", null ],
    [ "app_lib_data_reserved_f", "d2/dbd/data_8h.html#a542371eba82d3ee93c7ad3e9900fe3e7", null ],
    [ "app_lib_data_send_data_f", "d2/dbd/data_8h.html#a9ae6095889898db72257eeca385eee7b", null ],
    [ "app_lib_data_set_bcast_data_received_cb_f", "d2/dbd/data_8h.html#a2c6a654cd8fe208b90c102ffec3e0071", null ],
    [ "app_lib_data_set_data_received_cb_f", "d2/dbd/data_8h.html#a108548eef01aa6b4e3db667ef3c1c780", null ],
    [ "app_lib_data_set_data_sent_cb_f", "d2/dbd/data_8h.html#a602aad54057eaa2a5afb26888e9b1298", null ],
    [ "app_lib_data_set_max_msg_queuing_time_f", "d2/dbd/data_8h.html#a3c836eac2cb4cfc2df204f3c5dd51e81", null ],
    [ "app_lib_data_set_new_app_config_cb_f", "d2/dbd/data_8h.html#abcc8bb343266f3fc98d25590ed9a66a6", null ],
    [ "app_lib_data_tracking_id_t", "d2/dbd/data_8h.html#a3a8535cade8b081cfee20f0f4b9b0e65", null ],
    [ "app_lib_data_write_app_config_f", "d2/dbd/data_8h.html#a9bf5c1ad44fb5c03ed5ed2625371e5f9", null ],
    [ "app_lib_data_app_config_res_e", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5", [
      [ "APP_LIB_DATA_APP_CONFIG_RES_SUCCESS", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5a3237bc91d2d3122da0b4f8323e8a8993", null ],
      [ "APP_LIB_DATA_APP_CONFIG_RES_INVALID_ROLE", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5aafb56658f9fc3bb9be090aff91b10188", null ],
      [ "APP_LIB_DATA_APP_CONFIG_RES_INVALID_APP_CONFIG", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5aff1d36228852aef7252f370bb39bdc01", null ],
      [ "APP_LIB_DATA_APP_CONFIG_RES_INVALID_SEQ", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5acb59466353d5324cffe17afa496844c4", null ],
      [ "APP_LIB_DATA_APP_CONFIG_RES_INVALID_INTERVAL", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5af4dc7da81f898934f9127869aacd37b0", null ],
      [ "APP_LIB_DATA_APP_CONFIG_RES_INVALID_NULL_POINTER", "d2/dbd/data_8h.html#af4e3fdf65e16c2dc9ae1aa415d541bb5a99898fcc462142af4e21f8e834681218", null ]
    ] ],
    [ "app_lib_data_qos_e", "d2/dbd/data_8h.html#aa03fbf2cdb28616e79aca69710c8b134", [
      [ "APP_LIB_DATA_QOS_NORMAL", "d2/dbd/data_8h.html#aa03fbf2cdb28616e79aca69710c8b134a50d99e5f118f1d4293ef31094831678f", null ],
      [ "APP_LIB_DATA_QOS_HIGH", "d2/dbd/data_8h.html#aa03fbf2cdb28616e79aca69710c8b134a2c19a7699397df7b39155e4711bb762e", null ]
    ] ],
    [ "app_lib_data_receive_res_e", "d2/dbd/data_8h.html#ad3aa19c96da7da312cd8fe99ea34554b", [
      [ "APP_LIB_DATA_RECEIVE_RES_HANDLED", "d2/dbd/data_8h.html#ad3aa19c96da7da312cd8fe99ea34554ba5eb2a589b49f72b477f0cbf5deba552a", null ],
      [ "APP_LIB_DATA_RECEIVE_RES_NOT_FOR_APP", "d2/dbd/data_8h.html#ad3aa19c96da7da312cd8fe99ea34554ba982c231319cbb17347b7a0964988b855", null ],
      [ "APP_LIB_DATA_RECEIVE_RES_NO_SPACE", "d2/dbd/data_8h.html#ad3aa19c96da7da312cd8fe99ea34554ba0f705218bd4bea8dd628af25485e786f", null ]
    ] ],
    [ "app_lib_data_send_flags_e", "d2/dbd/data_8h.html#a45089cc0ed8195699c2cb52c160e9b44", [
      [ "APP_LIB_DATA_SEND_FLAG_NONE", "d2/dbd/data_8h.html#a45089cc0ed8195699c2cb52c160e9b44a35ac295081a6f0df8a7fb599d890993f", null ],
      [ "APP_LIB_DATA_SEND_FLAG_TRACK", "d2/dbd/data_8h.html#a45089cc0ed8195699c2cb52c160e9b44a7cf3634804cb75b3c58960b082d8852f", null ],
      [ "APP_LIB_DATA_SEND_SET_HOP_LIMITING", "d2/dbd/data_8h.html#a45089cc0ed8195699c2cb52c160e9b44a4ca3b9e595b765bec22c43c744bbf5b8", null ],
      [ "APP_LIB_DATA_SEND_FLAG_UNACK_CSMA_CA", "d2/dbd/data_8h.html#a45089cc0ed8195699c2cb52c160e9b44a47e2006b3eeca185dd7a7b3c8f30b3b4", null ]
    ] ],
    [ "app_lib_data_send_res_e", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164", [
      [ "APP_LIB_DATA_SEND_RES_SUCCESS", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164acf63322bd1c34794b230e2120116cfbb", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_STACK_STATE", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164a17ae3452dfa545699fe3e5da293ba8f7", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_QOS", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164ac49f54856cdbf8930b84161cc079c722", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_FLAGS", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164afd6a35958698f159339268ac3c173d0b", null ],
      [ "APP_LIB_DATA_SEND_RES_OUT_OF_MEMORY", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164abc148375192b9cd13d5a3733def48cf6", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_DEST_ADDRESS", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164a5a4b7dbb7517d5f7d85930d66b035fc5", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_NUM_BYTES", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164a3a6a3061dbc007dbd45d957fe0ed0baf", null ],
      [ "APP_LIB_DATA_SEND_RES_OUT_OF_TRACKING_IDS", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164abcd97cecaeb16255d18afbd559fd6da6", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_TRACKING_ID", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164ae41182cc2dfd9f1a9ebeb455665c2efb", null ],
      [ "APP_LIB_DATA_SEND_RES_RESERVED_ENDPOINT", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164a16ff7dbaabef087694510f484c88d725", null ],
      [ "APP_LIB_DATA_SEND_RES_ACCESS_DENIED", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164ad596c53ca64be97c5a4ea740c27cd2e8", null ],
      [ "APP_LIB_DATA_SEND_RES_INVALID_HOP_LIMIT", "d2/dbd/data_8h.html#ab53b6510f8ecb4a1621c905c15855164ad6581bc7a9d06eb66e421d47158e48e3", null ]
    ] ]
];