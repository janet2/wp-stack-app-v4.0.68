var structapp__lib__beacon__tx__t =
[
    [ "clearBeacons", "d2/d19/structapp__lib__beacon__tx__t.html#ab6d0099bf785c7a3c86bd062f62385e7", null ],
    [ "enableBeacons", "d2/d19/structapp__lib__beacon__tx__t.html#a854877eb20c41091977a984922765c25", null ],
    [ "setBeaconChannels", "d2/d19/structapp__lib__beacon__tx__t.html#ad82d9b700211bc66d4008750a0a62600", null ],
    [ "setBeaconContents", "d2/d19/structapp__lib__beacon__tx__t.html#a4f036b58c364bc9e18f65a34ec662a82", null ],
    [ "setBeaconInterval", "d2/d19/structapp__lib__beacon__tx__t.html#a68a7e15245801d62c1b730e82461f48c", null ],
    [ "setBeaconPower", "d2/d19/structapp__lib__beacon__tx__t.html#ae9d975c227ef844b4481014f6eb910b6", null ]
];