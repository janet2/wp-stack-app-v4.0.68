var structapp__lib__otap__remote__status__t =
[
    [ "area_id", "d2/dd9/structapp__lib__otap__remote__status__t.html#ab6b52d9b7d7fe6e4a638e27c56587063", null ],
    [ "crc", "d2/dd9/structapp__lib__otap__remote__status__t.html#aa60093a9a5d5d17864cfda66c47733e3", null ],
    [ "devel_version", "d2/dd9/structapp__lib__otap__remote__status__t.html#a91d054c645364583a95f73b5d05d794b", null ],
    [ "maint_version", "d2/dd9/structapp__lib__otap__remote__status__t.html#af121746491bc48d9dd1e51f0bf7b601b", null ],
    [ "major_version", "d2/dd9/structapp__lib__otap__remote__status__t.html#a79fca74d233fe79b334ecc712ca688ea", null ],
    [ "minor_version", "d2/dd9/structapp__lib__otap__remote__status__t.html#a384e7593c5c4c33e1f7c9936dab0b937", null ],
    [ "num_bytes", "d2/dd9/structapp__lib__otap__remote__status__t.html#af7dcc80b7a3cab03843f5bdff120f452", null ],
    [ "processed_crc", "d2/dd9/structapp__lib__otap__remote__status__t.html#a58dda01cd593d3f0f71cdacbef56de92", null ],
    [ "processed_num_bytes", "d2/dd9/structapp__lib__otap__remote__status__t.html#aca4d14a7b32b471345679b84e09a598e", null ],
    [ "processed_seq", "d2/dd9/structapp__lib__otap__remote__status__t.html#af7cdee056e7ce593284b008e093aafc6", null ],
    [ "seq", "d2/dd9/structapp__lib__otap__remote__status__t.html#ac10ce9ad3b95efd931a5073fcf0b1581", null ],
    [ "source", "d2/dd9/structapp__lib__otap__remote__status__t.html#a06473061cfd32407c6f56c502afaf70f", null ],
    [ "status", "d2/dd9/structapp__lib__otap__remote__status__t.html#ade20423e91627f07e610924cb0081623", null ],
    [ "type", "d2/dd9/structapp__lib__otap__remote__status__t.html#a1d127017fb298b889f4ba24752d08b8e", null ],
    [ "update_req_timeout", "d2/dd9/structapp__lib__otap__remote__status__t.html#afe9710c16ac91e2eb61ed492713b086f", null ]
];