var structi2c__xfer__t =
[
    [ "address", "df/d55/structi2c__xfer__t.html#af3f726014b044194def151079f1f2d89", null ],
    [ "custom", "df/d55/structi2c__xfer__t.html#a305d057014ab1133f4355dbe493868a4", null ],
    [ "read_ptr", "df/d55/structi2c__xfer__t.html#a3819982be3076624007198e0c02cd02c", null ],
    [ "read_size", "df/d55/structi2c__xfer__t.html#a7164bd664ab18ea8f706b9a2fad88d79", null ],
    [ "write_ptr", "df/d55/structi2c__xfer__t.html#aa120f31a7eda3152292a93a23b907c18", null ],
    [ "write_size", "df/d55/structi2c__xfer__t.html#a4c1e61cc44bf1a07f289f2f68bd260bf", null ]
];