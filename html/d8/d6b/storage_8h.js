var storage_8h =
[
    [ "app_lib_storage_t", "d3/d48/structapp__lib__storage__t.html", "d3/d48/structapp__lib__storage__t" ],
    [ "APP_LIB_STORAGE_NAME", "d8/d6b/storage_8h.html#aa5464fafc557e656b1ea5d2b38cfe3b1", null ],
    [ "APP_LIB_STORAGE_VERSION", "d8/d6b/storage_8h.html#a97d1f371d2268a15e3babe13080d1e1a", null ],
    [ "app_lib_storage_get_persistent_max_size_f", "d8/d6b/storage_8h.html#a77b56196275da84f0b4f8ef676adfc9d", null ],
    [ "app_lib_storage_read_persistent_f", "d8/d6b/storage_8h.html#a841128850e10194b33e0c6cd503ae04d", null ],
    [ "app_lib_storage_write_persistent_f", "d8/d6b/storage_8h.html#a4b5cf252701e4faee39fbc81e3386c62", null ]
];