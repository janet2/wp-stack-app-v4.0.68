var structapp__lib__memory__area__t =
[
    [ "getAreaInfo", "d8/d55/structapp__lib__memory__area__t.html#a315eaca5f84b1f4cf42ea15ed82e32ff", null ],
    [ "getAreaList", "d8/d55/structapp__lib__memory__area__t.html#a3b3cb5e6c2fd3cf771bf4cd425ef7572", null ],
    [ "isBusy", "d8/d55/structapp__lib__memory__area__t.html#a2f56c8dbbd4e7697da7c19610ca65948", null ],
    [ "startErase", "d8/d55/structapp__lib__memory__area__t.html#a06a1b388b6cdde85da24fd08be4c8569", null ],
    [ "startRead", "d8/d55/structapp__lib__memory__area__t.html#a778d8f2d9fcf8e27aa4bf9d49de3f94f", null ],
    [ "startWrite", "d8/d55/structapp__lib__memory__area__t.html#af03b67de726290443507938067e1a5d1", null ]
];