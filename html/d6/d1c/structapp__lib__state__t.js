var structapp__lib__state__t =
[
    [ "getAccessCycle", "d6/d1c/structapp__lib__state__t.html#a102ebd48fb4fe9465cb0be239b6c6131", null ],
    [ "getDiagInterval", "d6/d1c/structapp__lib__state__t.html#a9d5a8caf2ff52aa8bde8c4efc6f4e7dd", null ],
    [ "getEnergy", "d6/d1c/structapp__lib__state__t.html#ab2f3d28cd2e66d7aec551063d0985f9d", null ],
    [ "getNbors", "d6/d1c/structapp__lib__state__t.html#ac9df9a80c1a0436c905becf059d42140", null ],
    [ "getRouteCount", "d6/d1c/structapp__lib__state__t.html#ab925de033c92a5e6dcf7003dd0bd9099", null ],
    [ "getSinkCost", "d6/d1c/structapp__lib__state__t.html#a56b210c006bde33677ea063613e601d8", null ],
    [ "getStackState", "d6/d1c/structapp__lib__state__t.html#ac17a6229495f670e628bc37d8078fc3f", null ],
    [ "setEnergy", "d6/d1c/structapp__lib__state__t.html#aaef7c03a0b02d86d78ec7bccacacc4f2", null ],
    [ "setOnBeaconCb", "d6/d1c/structapp__lib__state__t.html#a209670d47cfe0e627f4abd3b5dc2d9cb", null ],
    [ "setOnScanNborsCb", "d6/d1c/structapp__lib__state__t.html#ad714526ae1f1bf0d8cd0effcd1f5848f", null ],
    [ "setSinkCost", "d6/d1c/structapp__lib__state__t.html#ad387be6eccd064044799cf2fecaab303", null ],
    [ "startScanNbors", "d6/d1c/structapp__lib__state__t.html#aba04929155b94d47efba6d825956798d", null ],
    [ "startStack", "d6/d1c/structapp__lib__state__t.html#a6179954360d3375cc35175bf915f2b05", null ],
    [ "stopStack", "d6/d1c/structapp__lib__state__t.html#a4f55f4c19aade4513ce938d9a13c1689", null ]
];