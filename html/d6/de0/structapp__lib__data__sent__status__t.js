var structapp__lib__data__sent__status__t =
[
    [ "dest_address", "d6/de0/structapp__lib__data__sent__status__t.html#abe388560a25c7690c48b6f3e20a32278", null ],
    [ "dest_endpoint", "d6/de0/structapp__lib__data__sent__status__t.html#a0992896b3999455a300e8ba25c368ae8", null ],
    [ "queue_time", "d6/de0/structapp__lib__data__sent__status__t.html#a39f101eb4be9d5ac0ec9ad59778252bf", null ],
    [ "src_endpoint", "d6/de0/structapp__lib__data__sent__status__t.html#a2f980290790d1fecb3764ef797c03335", null ],
    [ "success", "d6/de0/structapp__lib__data__sent__status__t.html#a7960f9c558f9ee2c3d4a8fdea096fb56", null ],
    [ "tracking_id", "d6/de0/structapp__lib__data__sent__status__t.html#a7f43082a8111a69a9189426ea4fb9987", null ]
];