var NAVTREE =
[
  [ "Wirepas Single-MCU SDK", "index.html", [
    [ "Single-MCU Operation Overview", "index.html", [
      [ "Application firmware", "index.html#application_firmware", null ],
      [ "Application-specific Hardware Abstraction Layer (HAL)", "index.html#application_hal", null ],
      [ "Wirepas Mesh stack", "index.html#stack", null ],
      [ "Wirepas Mesh Single-MCU API", "index.html#single_mcu_api", null ],
      [ "The physical layer (PHY)", "index.html#phy", null ]
    ] ],
    [ "Application API", "dd/d9c/programming_interface.html", [
      [ "Application API", "dd/d9c/programming_interface.html#application_api", [
        [ "Application startup", "dd/d9c/programming_interface.html#app_init", null ],
        [ "Application and Library", "dd/d9c/programming_interface.html#application_and_library_versioning", null ]
      ] ]
    ] ],
    [ "Application Examples", "d5/d7b/application_examples.html", null ],
    [ "Wirepas Mesh Concepts", "d5/df0/concepts.html", [
      [ "Application Configuration Data", "d5/df0/concepts.html#appconfig", null ],
      [ "Node addressing", "d5/df0/concepts.html#addressing", null ],
      [ "Source and Destination Endpoints", "d5/df0/concepts.html#endpoint", null ],
      [ "Communication Directions", "d5/df0/concepts.html#direction", null ]
    ] ],
    [ "How to Develop Application with SDK", "de/d7a/how_to_develop.html", [
      [ "How to create and test a custom application?", "de/d7a/how_to_develop.html#how_to_create_and_test_a_custom_application", [
        [ "Build", "de/d7a/how_to_develop.html#build", null ],
        [ "Test application", "de/d7a/how_to_develop.html#test_application", null ],
        [ "Flashing the device", "de/d7a/how_to_develop.html#flashing_the_device", null ],
        [ "Using OTAP", "de/d7a/how_to_develop.html#using_otap", null ]
      ] ],
      [ "Development of a new application", "de/d7a/how_to_develop.html#development_of_a_new_application", [
        [ "Copy of an application example", "de/d7a/how_to_develop.html#copy_of_an_application_example", null ],
        [ "Change default network", "de/d7a/how_to_develop.html#change_default_network_address_and_channel", null ],
        [ "Change of app_area_id", "de/d7a/how_to_develop.html#change_of_app_area_id", null ],
        [ "Helper function for API libraries", "de/d7a/how_to_develop.html#helper_function_for_API_libraries", null ],
        [ "Configuration of a node", "de/d7a/how_to_develop.html#configuration_of_a_node", null ],
        [ "Adding new source files", "de/d7a/how_to_develop.html#adding_new_source_files_to_the_application", null ]
      ] ],
      [ "Recommendations", "de/d7a/how_to_develop.html#recommendations", [
        [ "Security for scratchpad images", "de/d7a/how_to_develop.html#security_for_scratchpad_images", null ],
        [ "Optimization of network", "de/d7a/how_to_develop.html#optimization_of_network_throughput", null ],
        [ "Free resources", "de/d7a/how_to_develop.html#free_resources", null ],
        [ "Power consumption", "de/d7a/how_to_develop.html#power_consumption", null ]
      ] ]
    ] ],
    [ "Single-MCU API Operation Principle", "d7/d28/application_operation.html", [
      [ "Single-MCU API Operation Principle", "d7/d28/application_operation.html#operation_principle", null ],
      [ "Memory Partitioning", "d7/d28/application_operation.html#memory_partitioning", null ],
      [ "Application Detection", "d7/d28/application_operation.html#application_detection", null ],
      [ "Cooperative MCU Access", "d7/d28/application_operation.html#cooperative_mcu_access", [
        [ "Periodic Application Callback Function", "d7/d28/application_operation.html#periodic_application_callback_function", null ],
        [ "Asynchronous Application Callback Functions", "d7/d28/application_operation.html#asynchronous_application_callback_functions", null ],
        [ "Application Interrupt", "d7/d28/application_operation.html#application_interrupt", [
          [ "Deferred interrupt", "d7/d28/application_operation.html#deferred_interrupt", null ],
          [ "Fast interrupt", "d7/d28/application_operation.html#fast_interrupt", null ]
        ] ],
        [ "Which kind of interrupt for which purpose", "d7/d28/application_operation.html#which_kind_of_interrupt_for_which_purpose", null ],
        [ "Execution time limits", "d7/d28/application_operation.html#execution_time_limits", null ]
      ] ]
    ] ],
    [ "SDK Environment", "dd/d6a/sdk_environment.html", [
      [ "Installation of SDK Environment", "dd/d6a/sdk_environment.html#installation_of_sdk_environment", [
        [ "Windows installation", "dd/d6a/sdk_environment.html#windows_installation", null ],
        [ "Mac installation", "dd/d6a/sdk_environment.html#mac_installation", [
          [ "Homebrew", "dd/d6a/sdk_environment.html#homebrew", null ],
          [ "Python", "dd/d6a/sdk_environment.html#python", null ],
          [ "Install GCC Arm 4.8", "dd/d6a/sdk_environment.html#install_gcc_arm_48", null ],
          [ "Install Srecord", "dd/d6a/sdk_environment.html#install_srecord", null ],
          [ "install_segger_jlink_driver", "dd/d6a/sdk_environment.html#install_segger_jlink_driver", null ],
          [ "Install Nordic Semiconductor Command line tools", "dd/d6a/sdk_environment.html#install_nordic_semiconductor_command_line_tools", null ],
          [ "Troubleshooting Python Version Conflicts", "dd/d6a/sdk_environment.html#troubleshooting", null ]
        ] ]
      ] ],
      [ "Checking the installation validity", "dd/d6a/sdk_environment.html#checking_the_installation_validity", null ],
      [ "Flashing devices", "dd/d6a/sdk_environment.html#flashing_guideline", [
        [ "Flashing Nordic nRF52 devices", "dd/d6a/sdk_environment.html#flashing_nordic_nrf52_devices", null ],
        [ "Flashing Silicon Labs Devices", "dd/d6a/sdk_environment.html#flashing_silabs_devices", null ]
      ] ],
      [ "SDK File structure", "dd/d6a/sdk_environment.html#sdk_file_structure", [
        [ "<code>makefile (./)</code>", "dd/d6a/sdk_environment.html#makefile", null ],
        [ "gcc_app.ld (./mcu/<mcu>/linker)", "dd/d6a/sdk_environment.html#gcc_app_ld", null ],
        [ "<library>.h (./api)", "dd/d6a/sdk_environment.html#library_h", null ],
        [ "start.c (./mcu/common/)", "dd/d6a/sdk_environment.html#start_c", null ],
        [ "app.c (./source/<app_name>/)", "dd/d6a/sdk_environment.html#app_c", null ],
        [ "config.mk (./)", "dd/d6a/sdk_environment.html#config_mk", null ],
        [ "genscratchpad.py (./tools)", "dd/d6a/sdk_environment.html#genscratchpad_py", null ],
        [ "board.h (./board/<board>/)", "dd/d6a/sdk_environment.html#board_h", null ],
        [ "mcu specific files (./mcu/<mcu>/)", "dd/d6a/sdk_environment.html#mcu_specific_files", null ]
      ] ],
      [ "Resources on Nordic nRF52832 & nRF52840", "dd/d6a/sdk_environment.html#nordic_nrf52832_nrf52840", [
        [ "Flash Memory", "dd/d6a/sdk_environment.html#flash_memory", null ],
        [ "RAM Memory", "dd/d6a/sdk_environment.html#ram_memory", null ],
        [ "Peripherals accessible by stack only", "dd/d6a/sdk_environment.html#peripherals_accessible_by_stack_only", null ],
        [ "Peripherals Shared between the stack and the application", "dd/d6a/sdk_environment.html#peripherals_shared_between_the_stack_and_the_application", null ],
        [ "Peripherals available for the application", "dd/d6a/sdk_environment.html#peripherals_available_for_the_application", null ]
      ] ],
      [ "Resources on EFR32xG12", "dd/d6a/sdk_environment.html#efr32xg12", [
        [ "Flash Memory", "dd/d6a/sdk_environment.html#flash_memory2", null ],
        [ "RAM Memory", "dd/d6a/sdk_environment.html#ram_memory2", null ],
        [ "Peripherals accessible by stack only", "dd/d6a/sdk_environment.html#peripherals_accessible_by_stack_only2", null ],
        [ "Peripherals Shared between the stack and the application", "dd/d6a/sdk_environment.html#peripherals_shared_between_the_stack_and_the_application2", null ],
        [ "Peripherals available for the application", "dd/d6a/sdk_environment.html#peripherals_available_for_the_application2", null ]
      ] ]
    ] ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d3/dd6/structapp__lib__system__t.html#a7a48086cf5189cf13f4e67f569977d7e",
"d7/de1/hardware_8h.html#a5b162f218f49b1ca8651251750011fda",
"da/dd5/structapp__lib__beacon__rx__t.html#a0bb91e298163edbed33bd688612699db",
"dd/ddc/app__scheduler_8h.html#a245a7f2a478b72e2d4a43b3c18676ab3a1f4e0e040fb54d5088d0b57570eebcb0",
"functions_vars_h.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';