var structapp__lib__state__nbor__info__t =
[
    [ "address", "d1/db3/structapp__lib__state__nbor__info__t.html#ac0d31ca829f934cccd89f8054e02773e", null ],
    [ "channel", "d1/db3/structapp__lib__state__nbor__info__t.html#a715f5cb061d11eb75981741eda4dafcd", null ],
    [ "cost", "d1/db3/structapp__lib__state__nbor__info__t.html#a403b6f77447a921497e186aff3107823", null ],
    [ "last_update", "d1/db3/structapp__lib__state__nbor__info__t.html#ad4342e40daf8d6d7b1e7ef34bc9b332c", null ],
    [ "link_reliability", "d1/db3/structapp__lib__state__nbor__info__t.html#aa1c1e61cb5a2c63c5ea2df4bf954f35e", null ],
    [ "norm_rssi", "d1/db3/structapp__lib__state__nbor__info__t.html#a1ede7a83e9e8052506010057c6bf3353", null ],
    [ "pad", "d1/db3/structapp__lib__state__nbor__info__t.html#afe23e4c59de249880511510ea325a1ad", null ],
    [ "rx_power", "d1/db3/structapp__lib__state__nbor__info__t.html#a5394085ff97c98685b8c004dfac8c229", null ],
    [ "tx_power", "d1/db3/structapp__lib__state__nbor__info__t.html#a933494b037d387c3ebda7cdba7aee20b", null ],
    [ "type", "d1/db3/structapp__lib__state__nbor__info__t.html#a1d127017fb298b889f4ba24752d08b8e", null ]
];