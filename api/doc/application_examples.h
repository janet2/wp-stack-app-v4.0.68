
#ifndef _APPLICATION_EXAMPLES_H_
#define _APPLICATION_EXAMPLES_H_

/**

@page application_examples  Application Examples

This page lists all application examples and their purposes.

Applications exist in <code>source</code> subdirectly, subfolder is named after
application name. Note that not all applications exist for every processor
architecture.

<table>
<tr><th>Application name</th><th>Description</th><th>Notes</th></tr>
<tr><td>basic interrupt</td><td>How to use interrupt service</td><td>Only Nordic nRF52xx</td></tr>
<tr><td>ble_app</td><td>How to receive BLE beacons</td><td>Only 2.4GHz devices</td></tr>
<tr><td>ble_beacon</td><td>How to transmit BLE beacons</td><td>Only 2.4GHz devices</td></tr>
<tr><td>blink</td><td>Very simple blink/hello world application</td><td></td></tr>
<tr><td>bme280_example</td><td>How to use <a href="https://www.bosch-sensortec.com/bst/products/all_products/bme280">Bosch Sensortec BME280 environmental sensor</a> </td><td>Only Ruuvitag</td></tr>
<tr><td>custom_app</td><td>Simple data transmission and reception</td><td></td></tr>
<tr><td>dualmcu_app</td><td>Implementation of dual-MCU API interface, used most commonly in sinks</td><td></td></tr>
<tr><td>minimal_app</td><td>Minimal app that just starts the stack</td><td></td></tr>
<tr><td>nfc</td><td>NFC peripheral usage</td><td>Only Nordic nRF52xx</td></tr>
<tr><td>positioning_app</td><td>Application to acquire network data for the positioning use case</td><td>Only Nordic nRF52xx</td></tr>
<tr><td>pwm_driver</td><td>Create PWM signal</td><td>Only Nordic nRF52xx</td></tr>
<tr><td>ruuvi_evk</td><td>Send sensor data to backend, control how to send, used in evaluation kits</td><td>Only Ruuvitag</td></tr>
<tr><td>scheduler_example</td><td>How to use @ref app_scheduler.h "Application scheduler"</td><td></td></tr>
</table>

 */

#endif /* API_DOC_APPLICATION_EXAMPLES_H_ */
