#!/bin/bash

DEFAULT_TYPE="node"
DEFAULT_FIRMWARE_SINK="build/nrf52/migas-tr1-sink/final_image_migas-tr1-sink.hex"
DEFAULT_FIRMWARE_NODE="build/nrf52/migas-tr1-node/final_image_migas-tr1-node.hex"
DEFAULT_FIRMWARE=$DEFAULT_FIRMWARE_NODE
DEFAULT_CHANNEL=5
DEFAULT_ADDRESS=0xbeef00

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -t|--type)
    TYPE="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--id)
    ID="$2"
    shift # past argument
    shift # past value
    ;;
    -a|--address)
    ADDRESS="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--channel)
    CHANNEL="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--firmware)
    FIRMWARE="$2"
    shift # past argument
    shift # past value
    ;;
    # --default)
    # DEFAULT=YES
    # shift # past argument
    # ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters



# check parameters
if [ -z ${TYPE} ]; then
    TYPE=$DEFAULT_TYPE; echo "flashing type is ${TYPE}"
fi

if [ -z ${ID} ] && [ $TYPE == "node" ]; then
    echo "You should set node id!"; exit 1
fi

if [ $TYPE == "sink" ]; then
    DEFAULT_FIRMWARE=$DEFAULT_FIRMWARE_SINK;
elif [ $TYPE == "node" ]; then
    DEFAULT_FIRMWARE=$DEFAULT_FIRMWARE_NODE;
fi

if [ -z ${FIRMWARE} ]; then
    FIRMWARE=$DEFAULT_FIRMWARE; echo "use default firmware ${FIRMWARE}"
fi

if [ $TYPE == "node" ]; then
    if [ -z ${CHANNEL} ]; then
	CHANNEL=$DEFAULT_CHANNEL; echo "use default network channel ${CHANNEL}"
    fi

    if [ -z ${ADDRESS} ]; then
	ADDRESS=$DEFAULT_ADDRESS; echo "use default network address ${ADDRESS}"
    fi
fi

# recover
nrfjprog -f NRF52 --recover
if [ $? -ne 0 ]; then
	echo -e "\n  Error at Recover ..."
	echo -e "  Try Again."
    exit $?
fi

# flash the firmware
nrfjprog -f NRF52 --program $FIRMWARE --sectorerase
if [ $? -ne 0 ]; then
	echo -e "\n  Error at F/W Flashing ..."
	echo -e "  Try Again."
    exit $?
fi

if [ $TYPE == "node" ]; then
    echo NETWORK_ID  = "${ID}"
    echo NETWORK_ADDRESS     = "${ADDRESS}"
    echo NETWORK_CHANNEL    = "${CHANNEL}"

    nrfjprog --memwr 0x10001080 --val $ID -f NRF52
    if [ $? -ne 0 ]; then
	echo -e "\n  Error at Writing ID ..."
	echo -e "  Try Again."
	exit $?
    fi

    nrfjprog --memwr 0x10001084 --val $ADDRESS -f NRF52
    if [ $? -ne 0 ]; then
	echo -e "\n  Error at Writing Net-Address ..."
	echo -e "  Try Again."
	exit $?
    fi

    nrfjprog --memwr 0x10001088 --val $CHANNEL -f NRF52
    if [ $? -ne 0 ]; then
	echo -e "\n  Error at Writing Net-Channel ..."
	echo -e "  Try Again."
	exit $?
    fi
fi

nrfjprog --reset -f NRF52

echo -e "\n   Success !!!"
echo -e "   Do Next ..."
