#!/bin/bash

SCRAT_GEN="python tools/genscratchpad.py"
INI_FILE=tools/scratchpad_nrf52.ini

MCU=nrf52
IMAGE_PATH=image/$MCU
BUILD_DIR=build/$MCU

FIRMWARE_BIN=${IMAGE_PATH}/wpc_stack.hex
BOOTLOADER_BIN=${IMAGE_PATH}/bootloader.hex
FBS300_SINK_APP_BIN=${BUILD_DIR}/migas-tr1-sink/migas-tr1-sink.hex
FBS300_NODE_APP_BIN=${BUILD_DIR}/migas-tr1-node/migas-tr1-node.hex
FBS300_SINK_APP_VERSION=1.0.0.0
FBS300_NODE_APP_VERSION=1.0.0.0

FBS300_SEQUENCE_NUMBER=33
FBS300_APP_SCRATCHPAD_BIN=${BUILD_DIR}/migas-tr1-app.otap
FBS300_FULL_SCRATCHPAD_BIN=${BUILD_DIR}/migas-tr1-full.otap

fatal() {
    echo "$0: fatal error:" "$@" >&2
    exit 1
}

is_exist() {
    if [ $# = 0 ]; then
	fatal not enough arguments
    fi

    if [ ! -e $1 ]; then
	fatal $1 does not exist
    fi
}

fbs300_generate_app_scratchpad() {
    if [ $# != 4 ]; then
	fatal not enough arguments
    fi

    $SCRAT_GEN --type=combi \
	       --configfile=$INI_FILE \
	       --otapseq=$4 \
	       --set=APP_AREA_ID=0x846B7403 \
	       $1 \
	       $FBS300_SINK_APP_VERSION:0x846B7403:$2 \
	       $FBS300_NODE_APP_VERSION:0x83744C03:$3
}

fbs300_generate_full_scratchpad() {
    if [ $# != 6 ]; then
	fatal not enough arguments
    fi

    $SCRAT_GEN --type=combi \
	       --configfile=$INI_FILE \
	       --bootloader=$3 \
	       --otapseq=$6 \
	       --set=APP_AREA_ID=0x846B7403 \
	       $1 \
	       3.3.0.0:0x00000003:$2 \
	       $FBS300_SINK_APP_VERSION:0x846B7403:$4 \
	       $FBS300_NODE_APP_VERSION:0x83744C03:$5
}

for i in "$@"
do
case $i in
    -s=*|--sequencenumber=*)
    FBS300_SEQUENCE_NUMBER="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
            # unknown option
    ;;
esac
done

echo Seq $FBS300_SEQUENCE_NUMBER

is_exist $FBS300_SINK_APP_BIN
is_exist $FBS300_NODE_APP_BIN

#fbs300_generate_app_scratchpad $FBS300_APP_SCRATCHPAD_BIN \
#			       $FBS300_SINK_APP_BIN \
#			       $FBS300_NODE_APP_BIN \
#			       $FBS300_SEQUENCE_NUMBER

is_exist $FIRMWARE_BIN
is_exist $BOOTLOADER_BIN

fbs300_generate_full_scratchpad $FBS300_FULL_SCRATCHPAD_BIN \
				$FIRMWARE_BIN \
				$BOOTLOADER_BIN \
				$FBS300_SINK_APP_BIN \
				$FBS300_NODE_APP_BIN \
				$FBS300_SEQUENCE_NUMBER
