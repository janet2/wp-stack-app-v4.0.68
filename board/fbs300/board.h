/* Copyright 2017 Flyingloft. All Rights Reserved.
 */

#ifndef BOARD_FBS300_BOARD_H_
#define BOARD_FBS300_BOARD_H_

// Interrupt pin for waps uart
#define BOARD_UART_IRQ_PIN              12

// Serial port pins
#define BOARD_USART_TX_PIN              5
#define BOARD_USART_RX_PIN              4
#define BOARD_USART_CTS_PIN             9  /* For USE_USART_HW_FLOW_CONTROL */
#define BOARD_USART_RTS_PIN             10 /* For USE_USART_HW_FLOW_CONTROL */

// Pwm output for pwm_driver app
#define BOARD_PWM_OUTPUT_GPIO           28

#define BOARD_I2C_SCL_PIN               30
#define BOARD_I2C_SDA_PIN               31

#define BOARD_GPIO11_PIN                11 /* For Door Opening */

#endif /* BOARD_FBS300_BOARD_H_ */
