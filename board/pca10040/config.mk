# Mcu of the board
MCU=nrf52
MCU_SUB=

# This board supports BLE_beacon
CFLAGS += -DWITH_BEACON

# This board uses DCDC
HAL_POWER=yes