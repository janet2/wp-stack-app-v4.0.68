/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
*
* LIMITED WARRANTY – This software and documentation are strictly provided 
* on an  "as is" basis without any warranty as to their performance, 
* merchantability or fitness for any particular purpose. The user may use 
* the software as such or modify the software. Permission is hereby granted 
* to use, copy, and modify this application source code and host library, or 
* portions hereof, and documentation, subject to the following restrictions:
*
*     (i) Notice of any changes or modifications to the files, including the 
*     date changes were made
*
*     (ii) Results derived from this application source code and host library 
*     may not use the name “Wirepas” without prior written permission of 
*     Wirepas Ltd.
*
*     (iii) Application source code and/or host library shall be solely used 
*     in connection with software and products of Wirepas Ltd, and/or by
*     virtue of a license agreement between Wirepas Ltd and the licensee.
*     All other use is strictly forbidden without prior written permission
*     of Wirepas Ltd.
* 
* Wirepas Ltd. has no obligation to provide maintenance, support, updates, 
* enhancements or modifications. The licensee assumes the entire risk as to 
* the quality and performance of the software. Wirepas Ltd. warrants that 
* the media on which the Program is furnished will be free from any defects 
* in materials. Exclusive remedy in the event of a defect is expressly 
* limited to the replacement of said media.
*
* In no event shall Wirepas Ltd. or anyone else who has been involved in the 
* creation, development, production, or delivery of this software be liable 
* for any direct, incidental or consequential damages, such as, but not 
* limited to, loss of anticipated profits, benefits, use, or data resulting 
* from the use of this software, or arising out of any breach of warranty.
*/
