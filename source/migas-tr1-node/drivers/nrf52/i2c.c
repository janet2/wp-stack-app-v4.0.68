/* #include <stdint.h> */
/* #include <stdbool.h> */
/* #include <string.h> */

#include "hal_api.h"
#include "board.h"
#include "i2c.h"

#define SDA_LOW()	(nrf_gpio_pin_write(BOARD_I2C_SDA_PIN, 0))
#define SDA_OPEN()	(nrf_gpio_pin_write(BOARD_I2C_SDA_PIN, 1))
#define SDA_READ	(nrf_gpio_pin_read(BOARD_I2C_SDA_PIN))

#define SCL_LOW()	(nrf_gpio_pin_write(BOARD_I2C_SCL_PIN, 0))
#define SCL_OPEN()	(nrf_gpio_pin_write(BOARD_I2C_SCL_PIN, 1))
#define SCL_READ	(nrf_gpio_pin_read(BOARD_I2C_SCL_PIN))

static int I2c_waitWhileClockStreching(uint8_t timeout)
{
    /* etError error = NO_ERROR; */
    int error = 0;
  
    nrf_gpio_cfg_input(BOARD_I2C_SCL_PIN, NRF_GPIO_PIN_NOPULL);
  
    while (SCL_READ == 0) {
        if (timeout-- == 0)
            return -1; /* TIMEOUT_ERROR */
        nrf_delay_us(1000);
    }
  
    nrf_gpio_cfg_output(BOARD_I2C_SCL_PIN);
  
    return error;
}

void I2c_init(void)
{
    nrf_gpio_cfg_output(BOARD_I2C_SCL_PIN);
    nrf_gpio_cfg(BOARD_I2C_SDA_PIN,
                 NRF_GPIO_PIN_DIR_OUTPUT,
                 NRF_GPIO_PIN_INPUT_DISCONNECT,
                 NRF_GPIO_PIN_NOPULL,
                 NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);

    SDA_OPEN();                 // I2C-bus idle mode SDA released
    SCL_OPEN();                 // I2C-bus idle mode SCL released
}

void I2c_startCondition(void)
{
    I2c_init();        // Multiple device is connected at one IIC bus.

    SDA_OPEN();
    nrf_delay_us(1);
    SCL_OPEN();
    nrf_delay_us(1);
    SDA_LOW();
    nrf_delay_us(10);          // hold time start condition
    SCL_LOW();
    nrf_delay_us(10);
}

void I2c_stopCondition(void)
{
    SCL_LOW();
    nrf_delay_us(1);
    SDA_LOW();
    nrf_delay_us(1);
    SCL_OPEN();
    nrf_delay_us(10);         // set-up time stop condition
    SDA_OPEN();
    nrf_delay_us(10);
}

int I2c_writeByte(uint8_t tx)
{
    /* etError error = NO_ERROR; */
    int error = 0;
    uint8_t mask;

    for (mask = 0x80; mask > 0; mask >>= 1) // shift bit for masking (8 times)
    {
        if ((mask & tx) == 0)
            SDA_LOW();        // masking txByte, write bit to SDA-Line
        else
            SDA_OPEN();
        nrf_delay_us(1);        // data set-up time (t_SU;DAT)
        SCL_OPEN();             // generate clock pulse on SCL
        nrf_delay_us(5);        // SCL high time (t_HIGH)
        SCL_LOW();
        nrf_delay_us(1);        // data hold time(t_HD;DAT)
    }
    SDA_OPEN();                 // release SDA-line
    SCL_OPEN();                 // clk #9 for ack
    nrf_delay_us(1);            // data set-up time (t_SU;DAT)
    if (SDA_READ)
        error = -1;             // check ack from i2c slave

    SCL_LOW();
    nrf_delay_us(20);           // wait to see byte package on scope

    /* if (error) printf("[error] WB 0x%x\r\n", error); */

    return error;                         // return error code
}

int I2c_readByte(uint8_t *rx, i2c_ack_e ack, uint8_t timeout)
{
    int error = 0;
    /* etError error = NO_ERROR; */
    uint8_t mask;

    *rx = 0x00;
    SDA_OPEN();                 // release SDA-line

    nrf_gpio_cfg_input(BOARD_I2C_SDA_PIN, NRF_GPIO_PIN_NOPULL);

    for (mask = 0x80; mask > 0; mask >>= 1) // shift bit for masking (8 times)
    {
        SCL_OPEN();             // start clock on SCL-line
        nrf_delay_us(1);        // clock set-up time (t_SU;CLK)
        error = I2c_waitWhileClockStreching(timeout);// wait while clock streching
        /* if (error) printf("[RE] 0x%x\r\n", error); */
        nrf_delay_us(3);        // SCL high time (t_HIGH)
        if (SDA_READ)
            *rx |= mask;    // read bit
        SCL_LOW();
        nrf_delay_us(1);        // data hold time(t_HD;DAT)
    }

    nrf_gpio_cfg(BOARD_I2C_SDA_PIN,
                 NRF_GPIO_PIN_DIR_OUTPUT,
                 NRF_GPIO_PIN_INPUT_DISCONNECT,
                 NRF_GPIO_PIN_NOPULL,
                 NRF_GPIO_PIN_S0D1,
                 NRF_GPIO_PIN_NOSENSE);

    if (ack == I2C_ACK)
        SDA_LOW();              // send acknowledge if necessary
    else
        SDA_OPEN();

    nrf_delay_us(1);            // data set-up time (t_SU;DAT)
    SCL_OPEN();                 // clk #9 for ack
    nrf_delay_us(5);            // SCL high time (t_HIGH)
    SCL_LOW();
    SDA_OPEN();                 // release SDA-line
    nrf_delay_us(20);           // wait to see byte package on scope

    /* if (error) printf("[error] RB 0x%x\r\n", error); */

    return error;               // return with no error
}

int I2c_generalCallReset(void)
{
    /* etError error; */
    int error;
  
    I2c_startCondition();
    error = I2c_writeByte(0x00);
    if (error == 0) 
        error = I2c_writeByte(0x06);
  
    return error;
}
