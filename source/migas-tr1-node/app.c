/* Copyright 2017 Flyingloft. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

/*
 * \file    app.c
 * \brief
 *
 */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "hal_api.h"
#include "io.h"
#include "mcu.h"
#include "sht3x.h"
#include "api.h"
#include "node_configuration.h"

/* Testing will print out temperature and humidity over UART */
/* Measure temperature and humidity in every 5 secs */
/* Printout only needed datas including time stamp in CSV style */
/* timestamp, temperature, humidity */
/* To do this test, one sink is needed to start measure timer. */
#define TEST_SENSOR 1

// Command id's, sent as unicast or broadcast (be cautious with broadcast!)
#define CMDID_MSG_END               0x00   // End of message characters
#define CMDID_MEASURE_INTERVAL      0x01
#define CMDID_TEMPERATURE_ON_OFF    0x02   //
#define CMDID_HUMIDITY_ON_OFF       0x03   //
#define CMDID_GET_APPVERSION        0xFB
#define CMDID_GET_CONFIG            0xFC
#define CMDID_SET_CONFIG            0xFD
#define CMDID_FACTORY_RESET         0xFE

// Header of message format
typedef struct __attribute__ ((packed)) {
    uint8_t type;    // Request type
    uint8_t length;  // Number of bytes in value
    uint8_t value[]; // Value, dependent on the request or response type
} cfg_command_t;

#define APDU_FORMAT_VERSION          1

#define APDU_SENSOR_TYPE_TEMPERATURE 1
#define APDU_SENSOR_TYPE_HUMIDITY    2

#define APDU_VALUE_TYPE_UINT8        1
#define APDU_VALUE_TYPE_UINT16       2
#define APDU_VALUE_TYPE_UINT32       3
#define APDU_VALUE_TYPE_UINT64       4
#define APDU_VALUE_TYPE_FLAOT        5

/** Period to send data */
#if TEST_SENSOR
#define DEFAULT_PERIOD_S    5
#else
#define DEFAULT_PERIOD_S    30
#endif

#define DEFAULT_PERIOD_US   (DEFAULT_PERIOD_S*1000*1000)

/** Time needed to execute the periodic work, in us */
#define EXECUTION_TIME_US  500
/* #define EXECUTION_TIME_US 400 */

#define UICR_ADDRESS        0x10001080

#if TEST_SENSOR
#define DEBUG_ENABLED 1
#else
#define DEBUG_ENABLED 1 
#endif

typedef struct __attribute__ ((packed)) {
    uint8_t  flag;
    uint8_t  network_channel;
    uint32_t network_address;
    uint32_t node_address;
} flo_config_t;

#if 0 // org
#define NODE_ROLE       app_lib_settings_create_role(APP_LIB_SETTINGS_ROLE_HEADNODE, \
                                                     APP_LIB_SETTINGS_ROLE_FLAG_AUTOROLE)

#elif 0 // max power : 6mA
#define NODE_ROLE		app_lib_settings_create_role(APP_LIB_SETTINGS_ROLE_HEADNODE, \
													 APP_LIB_SETTINGS_ROLE_FLAG_LL)
#else // none auto role : 
#define NODE_ROLE		app_lib_settings_create_role(APP_LIB_SETTINGS_ROLE_HEADNODE, 0)
													
#endif

/** Endpoint to receive the commands */
#define MSG_CMD_EP      10

/** Endpoint to send data */
#define DATA_EP         1

static flo_config_t config;

#if !TEST_SENSOR
/** Period to send measurements, in us */
static uint32_t period_us;
#endif

/* static uint16_t period_s; */
static uint8_t  temperature_onoff = 1;
static uint8_t  humidity_onoff = 1;

static void LOG(const char *in)
{
#if (DEBUG_ENABLED == 1)
	if (in) 
		Usart_sendBuffer(in, strlen(in));
#else
	;
#endif
}

#if (DEBUG_ENABLED == 1)
void LOG_RES(int res)
{
	switch (res) {
		case APP_LIB_DATA_SEND_RES_SUCCESS :
			LOG("APP_LIB_DATA_SEND_RES_SUCCESS\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_STACK_STATE:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_STACK_STATE\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_QOS:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_QOS\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_FLAGS:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_FLAGS\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_OUT_OF_MEMORY:
			LOG("APP_LIB_DATA_SEND_RES_OUT_OF_MEMORY\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_DEST_ADDRESS:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_DEST_ADDRESS\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_NUM_BYTES:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_NUM_BYTES\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_OUT_OF_TRACKING_IDS:
			LOG("APP_LIB_DATA_SEND_RES_OUT_OF_TRACKING_IDS\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_INVALID_TRACKING_ID:
			LOG("APP_LIB_DATA_SEND_RES_INVALID_TRACKING_ID\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_RESERVED_ENDPOINT:
			LOG("APP_LIB_DATA_SEND_RES_RESERVED_ENDPOINT\r\n");
			break;
		case APP_LIB_DATA_SEND_RES_ACCESS_DENIED:
			LOG("APP_LIB_DATA_SEND_RES_ACCESS_DENIED\r\n");
			break;
		default:
			break;
	}
}
#endif


/**
 * \brief   Read the value in decimal from a string
 * \param   bytes
 *          Pointer to the string
 * \param   num_bytes
 *          Number of bytes in the string
 * \return  The parsed value
 */
#if 0
static uint32_t get_value_from_string(const uint8_t * bytes,
                                      size_t num_bytes)
{
    uint32_t value = 0;
    while ((num_bytes--) > 0)
    {
        char c = (char)(*bytes++);
        if ((c < '0') || (c > '9'))
        {
            // Not a digit
            break;
        }
        value *= 10;
        value += c - '0';
    }

    return value;
}
#endif

#if DEBUG_ENABLED
static int uint32_to_string(uint32_t in_uint32, char * out_string)
{
	int len = 0;
	
	/*Max value of uint32 is 4,294,967,295 (ten chars)*/	
	*(out_string + 0) = (in_uint32 / 1000000000 % 10) + 0x30;
	*(out_string + 1) = (in_uint32 /  100000000 % 10) + 0x30;
	*(out_string + 2) = (in_uint32 /   10000000 % 10) + 0x30;
	*(out_string + 3) = (in_uint32 /    1000000 % 10) + 0x30;
	*(out_string + 4) = (in_uint32 /     100000 % 10) + 0x30;
	*(out_string + 5) = (in_uint32 /      10000 % 10) + 0x30;
	*(out_string + 6) = (in_uint32 /       1000 % 10) + 0x30;
	*(out_string + 7) = (in_uint32 /        100 % 10) + 0x30;
	*(out_string + 8) = (in_uint32 /         10 % 10) + 0x30;
	*(out_string + 9) = (in_uint32 /          1 % 10) + 0x30;

	len = 10;
	
	return len;
}

static int int32_to_string(int32_t in_int32, char * out_string)
{
	int len = 0;

	/* Range of value of int32 is  (ten chars)
		-2,147,483,648 ~
	    2,147,438,647 */

	if (in_int32 < 0) {
		*(out_string + 0) = '-';
		in_int32 *= -1;
	}
	else if (in_int32 == 0) {
		*(out_string + 0) = ' ';
	}
	else {
		*(out_string + 0) = '+';
	}
	
	*(out_string +  1) = (in_int32 / 1000000000 % 10) + 0x30;
	*(out_string +  2) = (in_int32 /  100000000 % 10) + 0x30;
	*(out_string +  3) = (in_int32 /   10000000 % 10) + 0x30;
	*(out_string +  4) = (in_int32 /    1000000 % 10) + 0x30;
	*(out_string +  5) = (in_int32 /     100000 % 10) + 0x30;
	*(out_string +  6) = (in_int32 /      10000 % 10) + 0x30;
	*(out_string +  7) = (in_int32 /       1000 % 10) + 0x30;
	*(out_string +  8) = (in_int32 /        100 % 10) + 0x30;
	*(out_string +  9) = (in_int32 /         10 % 10) + 0x30;
	*(out_string + 10) = (in_int32 /          1 % 10) + 0x30;
	    
	len = 11;
	
	return len;
}
#endif

static void set_config(flo_config_t *cfg)
{
    app_res_e res;

    res = lib_storage->writePersistent(cfg, sizeof(flo_config_t));
    if (res != APP_RES_OK)
        return;
}

static app_addr_t getNodeAddressFromUICR()
{
	return NRF_UICR->CUSTOMER[0] & 0x00ffffff;
}

static app_addr_t getNetworkAddressFromUICR()
{
	return NRF_UICR->CUSTOMER[1] & 0x00ffffff;
}

static uint8_t getNetworkChannelFromUICR()
{
	return NRF_UICR->CUSTOMER[2] & 0xff;
}

static void get_config(flo_config_t *cfg)
{
    flo_config_t onflash = { 0 };
    app_res_e res;

    res = lib_storage->readPersistent(&onflash, sizeof(flo_config_t));
    if (res != APP_RES_OK)
        return;

    if (onflash.flag) {
        cfg->node_address = getNodeAddressFromUICR();
        cfg->network_address = getNetworkAddressFromUICR();
        cfg->network_channel = getNetworkChannelFromUICR();
        if (onflash.flag == 0xff) {
            cfg->flag = 1;
            set_config(cfg);
        }
    } else {
        cfg->flag = 0;
        cfg->node_address = onflash.node_address;
        cfg->network_address = onflash.network_address;
        cfg->network_channel = onflash.network_channel;
    }
}

static void ht_sensor_read_temp_humid(float* temperature, float* humidity)
{
    etError   error;
    int errcnt = 0;
SENSOR_RETRY: // Sensor error, retrying sensing
    // Single shot measurement with polling and 50ms timeout
    error = SHT3X_GetTempAndHumi(temperature, humidity, REPEATAB_HIGH, MODE_CLKSTRETCH, 50);
    while (error != NO_ERROR) {
/* #if !TUNING_SENSIRION */
/*      XSTR_E("error == 0x%x, (%d times) \r\n", error, errcnt); */
/* #endif // TUNING_SENSIRION */
        if (++errcnt >= 5)
            break;

        /* nrf_delay_ms(70); */
        nrf_delay_us(1000 * 70);
        error = SHT3X_GetTempAndHumi(temperature, humidity, REPEATAB_HIGH, MODE_CLKSTRETCH, 50);
    }

    // Sensor error, retrying sensing
    if (*temperature == 130 && *humidity == 100) {
/* #if !TUNING_SENSIRION */
/*      XSTR("Sensor error, T=130, H=100 ==> retry\r\n"); */
/* #endif */
        if (++errcnt >= 7)
            return;

        /* nrf_delay_ms(70); */
        nrf_delay_us(1000 * 70);
        goto SENSOR_RETRY;
    }
	
#if DEBUG_ENABLED
    {
    	char strSensorData[64] = {0};
		int offset = 0;

		uint32_t nTimestamp = lib_time->getTimestampS();
		int32_t nTemperature = (int32_t)(*temperature * 100.0);
		int32_t nHumidity = (int32_t)(*humidity * 100.0);
		
		offset += uint32_to_string(nTimestamp, strSensorData + offset);
		*(strSensorData + offset) = ','; offset ++;
		*(strSensorData + offset) = ' '; offset ++;
		
		offset += int32_to_string(nTemperature, strSensorData + offset);
		*(strSensorData + offset) = ','; offset ++;
		*(strSensorData + offset) = ' '; offset ++;
		
		offset += int32_to_string(nHumidity, strSensorData + offset);

		LOG(strSensorData);
		LOG("\r\n");
		
	}
#endif
	
}


#if TEST_SENSOR

static uint32_t testReadPrintSensor(void)
{
	uint32_t ret = DEFAULT_PERIOD_US;
	float temperature, humidity;

	/* Just Print out */
	ht_sensor_read_temp_humid(&temperature, &humidity);

	return ret;
}
static void testSetPeriodicCb(void)
{
	temperature_onoff = 1;
	humidity_onoff = 1;

	LOG("Calling setPeriodicCb 001\r\n");
	lib_system->setPeriodicCb(
			testReadPrintSensor,	/* Func */
			DEFAULT_PERIOD_US,		/* Initial Delay */
			EXECUTION_TIME_US);		/* MAX Execution Time */
}

#else

static int make_apdu(uint8_t *data)
{
    float temperature, humidity;
    void *offset = data;
    int bytes = 0;
    uint8_t num_measurements = 0;

    ht_sensor_read_temp_humid(&temperature, &humidity);

    /* version */
    *((uint8_t *) offset) = APDU_FORMAT_VERSION;
    bytes += sizeof(uint8_t);
    offset += sizeof(uint8_t);

    /* timestamp */
    *((uint32_t *) offset) = lib_time->getTimestampS();
    bytes += sizeof(uint32_t);
    offset += sizeof(uint32_t);

    /* number of measurements */
    bytes += sizeof(uint8_t);
    offset += sizeof(uint8_t);

    if (temperature_onoff) {
        /* sensor type */
        *((uint16_t *) offset) = APDU_SENSOR_TYPE_TEMPERATURE;
        bytes += sizeof(uint16_t);
        offset += sizeof(uint16_t);

        /* value type */
        *((uint8_t *) offset) = APDU_VALUE_TYPE_FLAOT;
        bytes += sizeof(uint8_t);
        offset += sizeof(uint8_t);

        /* temperature (float) */
        *((float *) offset) = temperature;
        bytes += sizeof(float);
        offset += sizeof(float);
        num_measurements += 1;
    }

    if (humidity_onoff) {
        /* sensor type */
        *((uint16_t *) offset) = APDU_SENSOR_TYPE_HUMIDITY;
        bytes += sizeof(uint16_t);
        offset += sizeof(uint16_t);

        /* value type */
        *((uint8_t *) offset) = APDU_VALUE_TYPE_FLAOT;
        bytes += sizeof(uint8_t);
        offset += sizeof(uint8_t);

        /* humidity (float) */
        *((float *) offset) = humidity;
        bytes += sizeof(float);
        offset += sizeof(float);
        num_measurements += 1;
    }

    data[5] = num_measurements;

    return bytes;
}


/**
 * \brief   Periodic callback
 */
static uint32_t send_sensor_data(void)
{
    static uint8_t data[24] = { 0, };

	/* TX Power MAX */
	NRF_RADIO->TXPOWER = RADIO_TXPOWER_TXPOWER_Pos4dBm;

    // Create a data packet to send
    app_lib_data_to_send_t data_to_send;
    int bytes;

#if (TEST_SENSOR == 0)
	LOG("Periodic send_sensor_data == \r\n");
#endif

    bytes = make_apdu(data);
    if (bytes > 0) {
        data_to_send.bytes = (const uint8_t *) data;
        data_to_send.num_bytes = bytes;
        data_to_send.dest_address = APP_ADDR_ANYSINK;
        data_to_send.src_endpoint = DATA_EP;
        data_to_send.dest_endpoint = DATA_EP;
        data_to_send.qos = APP_LIB_DATA_QOS_HIGH;
        data_to_send.delay = 0;
        data_to_send.flags = APP_LIB_DATA_SEND_FLAG_NONE;
        data_to_send.tracking_id = APP_LIB_DATA_NO_TRACKING_ID;

        // Send the data packet
#if (TEST_SENSOR == 0)
		LOG("Periodic send_sensor_data ++\r\n");
#endif

#if 1 /* Work around for Packet Loss */
		int res = 0;
		res = res;

		res = lib_data->sendData(&data_to_send);
#if 0
#if (DEBUG_ENABLED == 1)
		LOG_RES(res);
#endif
		nrf_delay_us(1000 * 70);

		res = lib_data->sendData(&data_to_send);
#if (DEBUG_ENABLED == 1)
		LOG_RES(res);
#endif
		nrf_delay_us(1000 * 70);

		int nCnt = 0;
		while (lib_data->sendData(&data_to_send) != APP_LIB_DATA_SEND_RES_SUCCESS && nCnt ++ < 22) {
			nrf_delay_us(1000 * 500);
		}
#endif
#else

        if (lib_data->sendData(&data_to_send) == APP_LIB_DATA_SEND_RES_SUCCESS) {
#if (TEST_SENSOR == 0)
			LOG("Sending OK\r\n");
#endif
		}
		else {
#if (TEST_SENSOR == 0)
			LOG("Sending Error\r\n");
#endif
		}
#endif

#if (TEST_SENSOR == 0)
		LOG("Periodic send_sensor_data --\r\n");
#endif
    }

    // Inform the stack that this function should be called again in
    // period_us microseconds. By returning APP_LIB_SYSTEM_STOP_PERIODIC,
    // the stack won't call this function again.
    return period_us;
}

static void handleMeasurementInterval(uint16_t interval)
{
#if TEST_SENSOR
	return;
#else
	
#if !DEBUG_ENABLED
    uint32_t new_period = interval;
    period_us = (new_period > 0) ? new_period * 1000 * 1000 : APP_LIB_SYSTEM_STOP_PERIODIC;
#endif

	return;
#endif
}

static void handleTemperatureOnOff(uint8_t onoff)
{
    temperature_onoff = onoff;
}

static void handleHumidityOnOff(uint8_t onoff)
{
    humidity_onoff = onoff;
}

static void handleGetAppVersion(app_addr_t src_address)
{
    uint8_t data[5] = { 0 };
    app_lib_data_to_send_t data_to_send;

    data[0] = CMDID_GET_APPVERSION;
    data[1] = APP_MAJOR;
    data[2] = APP_MINOR;
    data[3] = APP_MAINTENANCE;
    data[4] = 0;             /* reserved as developer */

    data_to_send.bytes = (const uint8_t *) data;
    data_to_send.num_bytes = 5;
    data_to_send.dest_address = src_address;
    data_to_send.src_endpoint = MSG_CMD_EP;
    data_to_send.dest_endpoint = MSG_CMD_EP;
    data_to_send.qos = APP_LIB_DATA_QOS_HIGH;
    data_to_send.delay = 0;
    data_to_send.flags = APP_LIB_DATA_SEND_FLAG_NONE;
    data_to_send.tracking_id = APP_LIB_DATA_NO_TRACKING_ID;

	LOG("handleGetAppVersion send_sensor_data ++\r\n");
    lib_data->sendData(&data_to_send);
	LOG("handleGetAppVersion send_sensor_data --\r\n");
}

static void handleGetConfig(app_addr_t src_address)
{
    app_lib_data_to_send_t data_to_send;

    get_config(&config);

    data_to_send.bytes = (const uint8_t *) &config;
    data_to_send.num_bytes = sizeof(flo_config_t);
    data_to_send.dest_address = src_address;
    data_to_send.src_endpoint = MSG_CMD_EP;
    data_to_send.dest_endpoint = MSG_CMD_EP;
    data_to_send.qos = APP_LIB_DATA_QOS_HIGH;
    data_to_send.delay = 0;
    data_to_send.flags = APP_LIB_DATA_SEND_FLAG_NONE;
    data_to_send.tracking_id = APP_LIB_DATA_NO_TRACKING_ID;

	LOG("handleGetConfig send_sensor_data ++\r\n");
    lib_data->sendData(&data_to_send);
	LOG("handleGetConfig send_sensor_data --\r\n");
}

static void handleSetConfig(uint8_t *buffer, size_t len)
{
    memcpy(&config, buffer, sizeof(config));
    set_config(&config);

    /* restart the stack */
    lib_state->stopStack();
#if 0
    get_config(&config);
    if (OverrideNodeConfig(config.node_address,
                           NODE_ROLE,
                           config.network_address,
                           config.network_channel) != APP_RES_OK) { return; }
    lib_state->startStack();
#endif
}

static void handleFactoryReset(void)
{
    config.flag = 1;
    set_config(&config);

    /* restart the stack */
    lib_state->stopStack();
#if 0
    get_config(&config);
    if (OverrideNodeConfig(config.node_address,
                           NODE_ROLE,
                           config.network_address,
                           config.network_channel) != APP_RES_OK) { return; }
    lib_state->startStack();
#endif	
}

#define HANDLE_VALUE(type, cmd, func)        do {   \
    type v;                                         \
    memcpy(&v, (cmd)->value, (cmd)->length);        \
    func(v);                                        \
} while (0)

/**
 *
 */
static bool parseMessages(const app_lib_data_received_t * data)
{
    const uint8_t *msg = data->bytes;
    size_t length = data->num_bytes;
    uint8_t handled_length = 0;

    while (handled_length < length) {
        cfg_command_t *cmd = (cfg_command_t *) &msg[handled_length];

        switch (cmd->type) {
        case CMDID_MEASURE_INTERVAL:
            HANDLE_VALUE(uint16_t, cmd, handleMeasurementInterval);
            handled_length += (2 + cmd->length);
            break;

        case CMDID_TEMPERATURE_ON_OFF:
            HANDLE_VALUE(uint8_t, cmd, handleTemperatureOnOff);
            handled_length += (2 + cmd->length);
            break;

        case CMDID_HUMIDITY_ON_OFF:
            HANDLE_VALUE(uint8_t, cmd, handleHumidityOnOff);
            handled_length += (2 + cmd->length);
            break;

        case CMDID_GET_APPVERSION:
            handleGetAppVersion(data->src_address);
            handled_length += (2 + cmd->length);
            break;

        case CMDID_GET_CONFIG:
            handleGetConfig(data->src_address);
            handled_length += (2 + cmd->length);
            break;
        case CMDID_SET_CONFIG:
            handleSetConfig(cmd->value, cmd->length);
            handled_length += (2 + cmd->length);
            break;
        case CMDID_FACTORY_RESET:
            handleFactoryReset();
            handled_length += (2 + cmd->length);
            break;

        default:
            handled_length = length;
            return false;
        }
    }

    return true;
}

/**
 * \brief   Data reception callback
 * \param   data
 *          Received data, \ref app_lib_data_received_t
 * \return  Result code, \ref app_lib_data_receive_res_e
 */
static app_lib_data_receive_res_e dataReceivedCb(
    const app_lib_data_received_t * data)
{
    if (data->num_bytes < 1)
		LOG("dataRX - 0 \r\n");
        // Data was not for this application
        return APP_LIB_DATA_RECEIVE_RES_NOT_FOR_APP;

    switch (data->dest_endpoint) {
#ifdef FEATURE_DOOR_OPENING
        case 1:
			LOG("dataRX - 1 \r\n");
                nrf_gpio_pin_set(BOARD_GPIO11_PIN);
                nrf_delay_us(1000 * 500);
                nrf_gpio_pin_clear(BOARD_GPIO11_PIN);
                break;
#endif  /* FEATURE_DOOR_OPENING */

        case MSG_CMD_EP: {
			LOG("dataRX - MSG_CMD_EP \r\n");
            parseMessages(data);
#if 0
            // Parse decimal digits to a period value
            uint32_t new_period = get_value_from_string(data->bytes, data->num_bytes);
            period_us = (new_period > 0) ? new_period * 1000 * 1000 :
                                   APP_LIB_SYSTEM_STOP_PERIODIC;

            // Update the period now. The current period will be longer
            // (already elapsed time from last period + new full period).
            lib_system->setPeriodicCb(send_sensor_data,
                                      period_us,
                                      EXECUTION_TIME_US);
#endif
            break;
        }

        default:
			LOG("dataRX - default \r\n");
            return APP_LIB_DATA_RECEIVE_RES_NOT_FOR_APP;
    }

	LOG("dataRX - OK \r\n");
    // Data handled successfully
    return APP_LIB_DATA_RECEIVE_RES_HANDLED;
}

static void appConfigReceivedCb(const uint8_t *bytes,
                                uint8_t seq,
                                uint16_t interval)
{
    app_lib_system_periodic_cb_f func;

	LOG("appConfigReceivedCb ++ \r\n");

    handleTemperatureOnOff(*bytes);
    handleHumidityOnOff(*((uint8_t *) (bytes + 1)));
    handleMeasurementInterval(*((uint16_t *) (bytes + 2)));

    if (!temperature_onoff && !humidity_onoff) {
        func = NULL;
    } else {
        func = send_sensor_data;
    }

	if (func == NULL)
		LOG("calling setPeriodicCb == NULL \r\n");
	else
		LOG("calling setPeriodicCb == send_sensor_data \r\n");

    // Update the period now. The current period will be longer
    // (already elapsed time from last period + new full period).
    lib_system->setPeriodicCb(func, period_us,
                              EXECUTION_TIME_US);
	LOG("appConfigReceivedCb -- \r\n");
}

void appSystemShutdownCb(void)
{
	get_config(&config);
	if (OverrideNodeConfig(config.node_address,
							NODE_ROLE,
							config.network_address,
							config.network_channel) != APP_RES_OK) {
		
		return;
	}
}
#endif /* end of TEST_SENSOR */


/**
 * \brief   Initialization callback for application
 *
 * This function is called after hardware has been initialized but the
 * stack is not yet running.
 *
 */
void App_init(const app_global_functions_t * functions)
{
    /* Enable DCDC. Refer to the WP-PD-103. */
    NRF_POWER->DCDCEN = POWER_DCDCEN_DCDCEN_Enabled << POWER_DCDCEN_DCDCEN_Pos;

	/* TX Power MAX */
	NRF_RADIO->TXPOWER = RADIO_TXPOWER_TXPOWER_Pos4dBm;
	
    API_Open(functions);
    HAL_Open();
    Io_init();

	/* jake UART */
#if (DEBUG_ENABLED == 1)
	Usart_init(115200, UART_FLOW_CONTROL_NONE);
#endif

	LOG("App_init 001 : Seq 38 : HEADER_ROLE only \r\n");

    get_config(&config);

	LOG("App_init 002\r\n");

#if (DEBUG_ENABLED == 1)
	{
		char strTemp[32];

		if (config.network_address == 0xa1b2c8) {
			LOG("Network Addr == 0xa1b2c8 \r\n");
		}
		else {
			LOG("Network Addr is not a1b2c8\r\n");
		}

		strTemp[0] = config.network_channel + 0x30;
		strTemp[1] = 0x00;

		LOG("Network Ch == ");
		LOG(strTemp);
		LOG("\r\n");
	}
#endif

    // Basic configuration of the node with a unique node address
#if TEST_SENSOR
	configureNode(1,1,1);
	testSetPeriodicCb();

#else
    if (configureNode(config.node_address,
                      config.network_address,
                      config.network_channel) != APP_RES_OK)
    {
        // Could not configure the node
        // It should not happen except if one of the config value is invalid
        return;
    }

	LOG("App_init 003\r\n");
    if (lib_settings->setNodeRole(NODE_ROLE) != APP_RES_OK)
        return;

	LOG("App_init 004\r\n");
    // Set a periodic callback to be called after DEFAULT_PERIOD_US
    period_us = DEFAULT_PERIOD_US;
    /* lib_system->setPeriodicCb(send_sensor_data, */
    /*                           period_us, */
    /*                           EXECUTION_TIME_US); */

    // Set callback for received unicast messages
    lib_data->setDataReceivedCb(dataReceivedCb);
	LOG("App_init 005\r\n");

    // Set callback for received broadcast messages
    lib_data->setBcastDataReceivedCb(dataReceivedCb);
	LOG("App_init 006\r\n");

    lib_data->setNewAppConfigCb(appConfigReceivedCb);
	LOG("App_init 007\r\n");

	
	lib_system->setShutdownCb(appSystemShutdownCb);

#endif

    // Start the stack
    lib_state->startStack();

#if (DEBUG_ENABLED == 1)
	{
		uint8_t stack_stat = 0;
		stack_stat = lib_state->getStackState();
		
		if ((stack_stat & 0xff) == 0) {
			LOG("[OK] Stack All 0x00 \r\n");
		}
		else {
			if (stack_stat & 0x01)
				LOG("[NG] Stack stopped \r\n");
			else 
				LOG("[OK] Stack Running \r\n");

			if (stack_stat & 0x02)
				LOG("[NG] Net addr missing \r\n");
			else 
				LOG("[OK] Net addr set \r\n");

			if (stack_stat & 0x04)
				LOG("[NG] Node addr missing \r\n");
			else 
				LOG("[OK] Node addr set \r\n");
			
			if (stack_stat & 0x08)
				LOG("[NG] Net Ch missing \r\n");
			else 
				LOG("[OK] Net Ch set \r\n");
			
			if (stack_stat & 0x10)
				LOG("[NG] Role missing \r\n");
			else 
				LOG("[OK] Role set \r\n");
		}	
	}
	
	LOG("App_init 008 - init done \r\n");
#endif
}
