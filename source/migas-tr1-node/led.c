/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

/*
 * \file    led.c
 * \brief   A file that chooses board-specific LED functions
 */

#define pca10028 1
#define wirepasEZR32Kit 2
#define pca10040 3

#if TARGET_BOARD == pca10028
#include "led_pca10028.c"
#elif TARGET_BOARD == wirepasEZR32Kit
#include "led_wirepas_ezr32_kit.c"
#elif TARGET_BOARD == pca10040
#include "led_pca10040.c"
#else
#error Invalid target board
#endif
