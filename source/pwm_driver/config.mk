# Boards compatible with this app 
TARGET_BOARDS := pca10040 

# Define default network settings
default_network_address ?= 0x12CD34
default_network_channel ?= 10

# Define a specific application area_id
app_specific_area_id=0x8067A5

# App version
app_major=4
app_minor=0
app_maintenance=0
app_development=0
