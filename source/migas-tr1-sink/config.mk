# Specify supported target boards here
TARGET_BOARDS := pca10040 fbs300

# To define a specific app_area_id for this application uncomment area definition
# The 8 lower bits will be appended depending on hardware version
# app_specific_area_id=0x8CEC61
app_specific_area_id=0x846B74
