/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

/*
 * \file    app.c
 * \brief   This file is a template to Dual MCU API app for all paltforms
 */
#include <stdlib.h>
#include "hal_api.h"
#include "io.h"
#include "mcu.h"
#include "api.h"
#include "waps.h"
#include "node_configuration.h"


/** Node settings */
/* #define NETWORK_CHANNEL       7 */
/* #define NETWORK_ADDRESS       0xA1B2C7 */
#define NODE_ROLE             app_lib_settings_create_role(APP_LIB_SETTINGS_ROLE_SINK, 0)
#define ACCESS_CYCLE          2000 // default 2 s


/** The global functions */
const app_global_functions_t * global_func = NULL;


#ifdef WITH_FRAG
uint16_t m_min_sdu_size = 0;
uint16_t m_curr_sdu_size = 0;
#endif
// Interface config
const app_interface_config_s m_interface_config =
{
    .interface = APP_UART_INT,
    .baudrate = UART_BAUDRATE,
    .flow_ctrl = UART_FLOWCONTROL
};

app_lib_data_receive_res_e dataReceivedCb(const app_lib_data_received_t * data)
{
    app_lib_data_receive_res_e ret;
    ret = Waps_receiveUnicast(data->bytes,
                              data->num_bytes,
                              data->src_address,
                              data->src_endpoint,
                              data->dest_endpoint,
                              data->delay,
                              data->qos);
    return ret;
}

app_lib_data_receive_res_e broadcastDataReceivedCb(const app_lib_data_received_t * data)
{
    app_lib_data_receive_res_e ret;
    ret = Waps_receiveBcast(data->bytes,
                            data->num_bytes,
                            data->src_address,
                            data->src_endpoint,
                            data->dest_endpoint,
                            data->delay,
                            data->qos);
    return ret;
}


void newAppConfigCb(const uint8_t * bytes,
                    uint8_t seq,
                    uint16_t interval)
{
    Waps_sinkUpdated(seq, bytes, interval);
}

static void dataSentCb(const app_lib_data_sent_status_t * status)
{
    Waps_packetSent(status->tracking_id,
                    status->src_endpoint,
                    status->dest_endpoint,
                    status->queue_time,
                    status->dest_address,
                    status->success);
}

static void remoteStatusReceivedCb(const app_lib_otap_remote_status_t * bl_status)
{
    Waps_receiveBlStatus(bl_status);
}
#ifdef WITH_FRAG
void sduReceivedCb(void)
{
    Waps_rcvSduInd();
}
#endif
void onScannedNborsCb(void)
{
    Waps_onScannedNbors();
}

/**
 * \brief   Initialization callback for application
 *
 * This function is called after hardware has been initialized but the
 * stack is not yet running.
 *
 */
void App_init(const app_global_functions_t * functions)
{
    global_func = functions;

    API_Open(functions);
    HAL_Open();
    Io_init();
    Waps_init();

    if (lib_state->getStackState() == APP_LIB_STATE_STARTED)
    {
        lib_state->stopStack();
    }

    /* lib_system->registerAppIrqTable(app_interrupt_table); */

    /* // Setup interrupt handlers */
    /* Wakeup_set_gpio_irq_handler(app_interrupt_table); */
    /* Usart_set_irq_handler(app_interrupt_table); */
    /* // Initialize IO's (enable clock and initialize pins) */
    /* Io_init(); */
    /* // Initialize deep sleep control module */
    /* DS_Init(); */


    /* /\* Basic configuration of the node with a unique node address *\/ */
    /* if (configureNode(100, /\* getUniqueAddress(), *\/ */
    /*               NODE_ROLE, */
    /*               NETWORK_ADDRESS, */
    /*               NETWORK_CHANNEL) != APP_RES_OK) */
    /* { */
    /*     // Could not configure the node */
    /*     // It should not happen except if one of the config value is invalid */
    /*     return; */
    /* } */

    //register callbacks
    lib_data->setDataReceivedCb(dataReceivedCb);
    lib_data->setBcastDataReceivedCb(broadcastDataReceivedCb);
    lib_data->setDataSentCb(dataSentCb);
    lib_data->setNewAppConfigCb(newAppConfigCb);
#ifdef WITH_FRAG
    m_min_sdu_size = lib_data->getDataMaxNumBytes() + 1;
    m_curr_sdu_size = m_min_sdu_size;
    lib_sdu->setSduReceivedCb(sduReceivedCb);
#endif
    lib_otap->setRemoteStatusReceivedCb(remoteStatusReceivedCb);
    lib_state->setOnScanNborsCb(onScannedNborsCb, 
			APP_LIB_STATE_SCAN_NBORS_ONLY_REQUESTED);

    // Set access cycle to 2 s
    lib_settings->setAcRange(ACCESS_CYCLE, ACCESS_CYCLE);

    /* lib_state->startStack(); */

	/* TX Power MAX */
	NRF_RADIO->TXPOWER = RADIO_TXPOWER_TXPOWER_Pos4dBm;
}
