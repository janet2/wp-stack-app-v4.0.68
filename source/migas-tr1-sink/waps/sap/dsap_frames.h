/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

#ifndef DSAP_FRAMES_H_
#define DSAP_FRAMES_H_

/** Result of transmitted packet */
typedef enum
{
    DSAP_TX_SUCCESS = 0,
    DSAP_TX_STACK_STOPPED = 1,
    DSAP_TX_INV_QOS_PARAM = 2,
    DSAP_TX_INV_OPTS_PARAM = 3,
    DSAP_TX_OUT_OF_MEMORY = 4,
    DSAP_TX_UNKNOWN_DST = 5,
    DSAP_TX_INV_LEN = 6,
    DSAP_TX_IND_FULL = 7,
    DSAP_TX_INV_PDU_ID = 8,
    DSAP_TX_RESV_EP = 9,
    DSAP_TX_ACCESS_DENIED = 10,
} dsap_tx_result_e;

typedef enum
{
    DSAP_IND_SUCCESS = 0,
    DSAP_IND_TIMEOUT = 1,
} dsap_indication_e;

/** TX options of WAPS_DSAP_DATA.request */
typedef enum
{
    TX_OPTS_NO_IND_REQ = 0, /** No indication req when pkt sent */
    TX_OPTS_IND_REQ = 1     /** Indication req */
} dsap_tx_options_e;

/** QOS classes of WAPS_DSAP_DATA.request */
typedef enum
{
    DSAP_QOS_NORMAL = 0,    /**< Normal priority packet */
    DSAP_QOS_HIGH = 1,      /**< High priority packet */
    DSAP_QOS_UNACKED = 2    /**< Unacknowledged packet */
} dsap_qos_e;

/** Maximum size of transmittable data APDU via WAPS/DSAP interface */
#define APDU_MAX_SIZE   102

/* WAPS-DSAP-DATA_TX-REQUEST */

typedef struct __attribute__ ((__packed__))
{
    pduid_t     apdu_id;
    ep_t        src_endpoint;
    w_addr_t    dst_addr;
    ep_t        dst_endpoint;
    uint8_t     qos;
    uint8_t     tx_opts;
    uint8_t     apdu_len;
    uint8_t     apdu[APDU_MAX_SIZE];
} dsap_data_tx_req_t;

#define FRAME_DSAP_DATA_TX_REQ_HEADER_SIZE  \
    (sizeof(dsap_data_tx_req_t) - APDU_MAX_SIZE)

/* WAPS-DSAP-DATA_TX_TT-REQUEST */

typedef struct __attribute__ ((__packed__))
{
    pduid_t     apdu_id;
    ep_t        src_endpoint;
    w_addr_t    dst_addr;
    ep_t        dst_endpoint;
    uint8_t     qos;
    uint8_t     tx_opts;
    uint32_t    travel_time;
    uint8_t     apdu_len;
    uint8_t     apdu[APDU_MAX_SIZE];
} dsap_data_tx_tt_req_t;

#define FRAME_DSAP_DATA_TX_TT_REQ_HEADER_SIZE  \
    (sizeof(dsap_data_tx_tt_req_t) - APDU_MAX_SIZE)

/* WAPS-DSAP-DATA_RX-INDICATION */

typedef struct __attribute__ ((__packed__))
{
    uint8_t     queued_indications;
    w_addr_t    src_addr;
    ep_t        src_endpoint;
    w_addr_t    dst_addr;
    ep_t        dst_endpoint;
    uint8_t     qos;
    uint32_t    delay;
    uint8_t     apdu_len;
    uint8_t     apdu[APDU_MAX_SIZE];
} dsap_data_rx_ind_t;

#define FRAME_DSAP_DATA_RX_IND_HEADER_SIZE  \
    (sizeof(dsap_data_rx_ind_t) - APDU_MAX_SIZE)

/* WAPS-DSAP-DATA_TX-INDICATION */

typedef struct __attribute__ ((__packed__))
{
    uint8_t     queued_indications;
    pduid_t     apdu_id;
    ep_t        src_endpoint;
    w_addr_t    dst_addr;
    ep_t        dst_endpoint;
    uint32_t    queue_delay;
    uint8_t     result;
} dsap_data_tx_ind_t;

/* WAPS-DSAP-DATA_TX-CONFIRMATION */

typedef struct __attribute__ ((__packed__))
{
    pduid_t     apdu_id;
    uint8_t     result;
    uint8_t     buff_cap;
} dsap_data_tx_cnf_t;

#ifdef WITH_FRAG
//SDU

/** Maximum size of transmittable data APDU via WAPS/DSAP interface */
#define APSDU_MAX_SIZE   240

/* WAPS-DSAP-DATA_TX-REQUEST */

typedef struct __attribute__ ((__packed__))
{
    w_addr_t    addr;
    uint8_t     payload_id;
    uint32_t    reserved;
    uint16_t    apsdu_len;
    uint8_t     apsdu[APSDU_MAX_SIZE];
} dsap_sdu_tx_req_t;

#define FRAME_DSAP_SDU_TX_REQ_HEADER_SIZE  \
    (sizeof(dsap_sdu_tx_req_t) - APSDU_MAX_SIZE)

/* WAPS-DSAP-SDU_RX-INDICATION */

typedef struct __attribute__ ((__packed__))
{
    uint8_t     queued_indications;
    w_addr_t    addr;
    uint8_t     payload_id;
    uint32_t    delay;
    uint16_t    apsdu_len;
    uint8_t     apsdu[APSDU_MAX_SIZE];
} dsap_sdu_rx_ind_t;

#define FRAME_DSAP_SDU_RX_IND_HEADER_SIZE  \
    (sizeof(dsap_sdu_rx_ind_t) - APSDU_MAX_SIZE)

/* WAPS-DSAP-SDU_TX-INDICATION */

typedef struct __attribute__ ((__packed__))
{
    uint8_t     queued_indications;
    w_addr_t    addr;
    uint8_t     payload_id;
    uint32_t    result;
    uint32_t    reserved;
} dsap_sdu_tx_ind_t;

/* WAPS-DSAP-SDU_TX-CONFIRMATION */

typedef struct __attribute__ ((__packed__))
{
    uint8_t     result;
    uint32_t    reserved;
} dsap_sdu_tx_cnf_t;
#endif

typedef union
{
    dsap_data_tx_req_t          data_tx_req;
    dsap_data_tx_tt_req_t       data_tx_tt_req;
    dsap_data_tx_ind_t          data_tx_ind;
    dsap_data_tx_cnf_t          data_tx_cnf;
    dsap_data_rx_ind_t          data_rx_ind;
#ifdef WITH_FRAG
    dsap_sdu_tx_req_t           sdu_tx_req;
    dsap_sdu_tx_ind_t           sdu_tx_ind;
    dsap_sdu_tx_cnf_t           sdu_tx_cnf;
    dsap_sdu_rx_ind_t           sdu_rx_ind;
#endif
} frame_dsap;


#endif /* DSAP_FRAMES_H_ */
