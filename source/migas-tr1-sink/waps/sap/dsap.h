/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

#ifndef WAPS_DSAP_H_
#define WAPS_DSAP_H_

#include "waps_private.h"
#include "waps_item.h"
#include "waddr.h"

/**
 *  \brief  Process received request
 *  \param  item
 *          Structure containing the received request frame
 *  \return True, if a response was generated
 */
bool Dsap_handleFrame(waps_item_t * item);

/**
 * \brief   Create a packet sent indication for sending to WAPS protocol
 * \param   id
 *          Tracking id for PDU
 * \param   src_ep
 *          Source end point
 * \param   dst_ep
 *          Destination end point
 * \param   queue_time
 *          Packet queuing time
 * \param   dst
 *          Packet destination address
 * \param   success
 *          True, if the packet was successfully sent
 * \param   output_ptr
 *          Memory area to construct the reply to
 */
void Dsap_packetSent(pduid_t id,
                     uint8_t src_ep,
                     uint8_t dst_ep,
                     uint32_t queue_time,
                     w_addr_t dst,
                     bool success,
                     waps_item_t * output_ptr);

/**
 * \brief   Create a packet received indication for sending to WAPS protocol
 * \param   bytes
 *          Data received
 * \param   size
 *          Amount of data received
 * \param   src_addr
 *          Source address of packet
 * \param   dst_addr
 *          Destination address of packet
 * \param   src_ep
 *          Source end point
 * \param   dst_ep
 *          Destination end point
 * \param   tc
 *          Traffic class of packet
 * \param   delay
 *          Propagation delay of packet
 * \param   output_ptr
 *          Memory area to construct the reply to
 */
void Dsap_packetReceived(const uint8_t * bytes,
                         uint8_t size,
                         w_addr_t src_addr,
                         w_addr_t dst_addr,
                         ep_t src_ep,
                         ep_t dst_ep,
                         uint8_t tc,
                         uint32_t delay,
                         waps_item_t * output_ptr);
#ifdef WITH_FRAG
/**
 * \brief   Create a packet received indication for sending to WAPS protocol
 * \param   bytes
 *          Data received
 * \param   size
 *          Amount of data received
 * \param   addr
 *          Source address of packet
 * \param   paylod_id
 *          Payload identifer of the packet
 * \param   output_ptr
 *          Memory area to construct the reply to
*/


void Dsap_sduPacketReceived(const uint8_t * bytes,
                            uint16_t size,
                            w_addr_t addr,
                            uint8_t payload_id,
                            uint32_t delay,
                            waps_item_t * output_ptr);
#endif

#endif /* WAPS_DSAP_H_ */
