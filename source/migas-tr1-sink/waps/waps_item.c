/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

#include <stdint.h>
#include "waps_item.h"
#include "api.h"

// Maximum amount of items
#ifdef CC2650
#define WAPS_MAX_ITEMS          8
#elif defined EFM32LG
#define WAPS_MAX_ITEMS          15
#elif defined NRF52
#define WAPS_MAX_ITEMS          42
#else
#define WAPS_MAX_ITEMS          16
#endif
#define WAPS_RESERVED_ITEMS     2
// Limit how many free items must be in the buffer in order to allow firmware to
// send new packets to application.
#define FREE_LIMIT              (WAPS_MAX_ITEMS / 2)

// Memory for items, make sure first element aligns properly
static waps_item_t __attribute__((aligned(4))) waps_item_bank[WAPS_MAX_ITEMS];

// List of usable (free) items
static sl_list_head_t           free_items;

void Waps_itemInit(void)
{
    // Initialize free items list
    sl_list_init(&free_items);
    // Clear items
    memset(&waps_item_bank[0], 0, sizeof(waps_item_bank));
    // Push items to free list
    for(uint32_t i = 0; i < WAPS_MAX_ITEMS; i++)
    {
        sl_list_push_front(&free_items, (sl_list_t *)&waps_item_bank[i]);
    }
}

waps_item_t * Waps_itemReserve(waps_item_type_e type)
{
    // Reserve item from free items
    waps_item_t * item = NULL;
    lib_system->enterCriticalSection();
    if((type == WAPS_ITEM_TYPE_REQUEST) ||
       (sl_list_size(&free_items) > WAPS_RESERVED_ITEMS))
    {
        // Either a request frame, or we have memory for new indication
        item = (waps_item_t *)sl_list_pop_front(&free_items);
    }
    lib_system->exitCriticalSection();
    return item;
}

void Waps_itemFree(waps_item_t * item)
{
    // Push item to free items list
    lib_system->enterCriticalSection();
    sl_list_push_front(&free_items, (sl_list_t *)item);
    lib_system->exitCriticalSection();
    // If there is enough room in the buffer, announce firmware to send new
    // packets
    if (sl_list_size(&free_items) >= FREE_LIMIT)
    {
        lib_data->allowReception(true);
    }
}
