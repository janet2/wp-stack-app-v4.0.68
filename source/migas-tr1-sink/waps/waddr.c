/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

#include "waddr.h"

app_addr_t Waddr_to_Addr(w_addr_t waddr)
{
    if(waddr == WADDR_BCAST)
    {
        return APP_ADDR_BROADCAST;
    }
    else if(waddr == WADDR_ANYSINK)
    {
        return APP_ADDR_ANYSINK;
    }
    // No domain conversion is necessary
    return waddr;
}

w_addr_t Addr_to_Waddr(app_addr_t app_addr)
{
    if(app_addr == APP_ADDR_BROADCAST)
    {
        return WADDR_BCAST;
    }
    else if(app_addr == APP_ADDR_ANYSINK)
    {
        return WADDR_ANYSINK;
    }
    // No domain conversion is necessary
    return app_addr;
}

bool Waddr_addrIsValid(w_addr_t addr)
{
    // Waddr has a valid range of 0...16777213
    if((addr >= (WADDR_MAX & 0x00FFFFFF)) && (addr != WADDR_BCAST))
    {
        return false;
    }
    return true;
}
