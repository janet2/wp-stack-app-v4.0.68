#ifndef I2C_H_
#define I2C_H_

#include <stdint.h>
#include "api.h"

typedef enum
{
    I2C_ACK = 0,
    I2C_NACK,
} i2c_ack_e;

void I2c_init(void);
void I2c_startCondition(void);
void I2c_stopCondition(void);
int I2c_writeByte(uint8_t tx);
int I2c_readByte(uint8_t *rx, i2c_ack_e ack, uint8_t timeout);
int I2c_generalCallReset(void);

#endif /* I2C_H_ */
