/* Copyright 2017 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

#ifndef SOURCE_WAPS_APP_SERVICES_H_
#define SOURCE_WAPS_APP_SERVICES_H_

#include "app.h"
#include "data.h"
#include "settings.h"
#include "state.h"
#include "system.h"
#include "storage.h"
#include "time.h"
#include "otap.h"

#endif /* SOURCE_WAPS_APP_SERVICES_H_ */
