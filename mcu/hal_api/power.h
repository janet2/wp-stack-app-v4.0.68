/* Copyright 2018 Wirepas Ltd. All Rights Reserved.
 *
 * See file LICENSE.txt for full license details.
 *
 */

/**
 * \brief   Generic power codes.
 */
#ifndef POWER_H_
#define POWER_H_

/**
 * \brief   Enable DCDC converter
 */
extern void Power_enableDCDC() __attribute__((weak));

#endif /* POWER_H_ */
