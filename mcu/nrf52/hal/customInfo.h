#ifndef NODEID_H
#define NODEID_H

#include "mcu.h"

__STATIC_INLINE uint32_t getNodeAddress()
{
    return NRF_UICR->CUSTOMER[0];
}

__STATIC_INLINE uint32_t getNetworkAddress()
{
    return NRF_UICR->CUSTOMER[1];
}

__STATIC_INLINE uint32_t getNetworkChannel()
{
    return NRF_UICR->CUSTOMER[2];
}

#endif /* NODEID_H */
