# Mcu instruction set
ARCH=armv7e-m

# This mcu has a bootloader (enough memory)
HAS_BOOTLOADER=yes

# Hardware magic used for this architecture
HW_MAGIC=03

# Add custom flags
# Remove the -Wunused-parameter flag added by -Wextra as some cortex M4 header do not respect it
CFLAGS += -Wno-unused-parameter

# This mcu uses the version 3 of the bootloader (with external flash support)
BOOTLOADER_VERSION=v3
